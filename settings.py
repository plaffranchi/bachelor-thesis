import os

ROOT = os.path.dirname(os.path.realpath(__file__))

DATA_ROOT_FOLDER = os.path.join(ROOT, "../data")
FIGURES_ROOT_FOLDER = os.path.join(ROOT, "../text/figures")

'''
These settings assume the following file structure:

- DATA_ROOT_FOLDER
    - Match folder 1
        - Tracking file (ending with "tracking.dat")
        - Meta file (ending with "meta.json")
    - Match folder 2
        - Tracking file (ending with "tracking.dat")
        - Meta file (ending with "meta.json")
        ...
    ...

By default, all npz objects used for cache are in the same folder as the match. 
If you want to put all npz objects in the same folder, make sure to specify a match_ic,
otherwise they may get overwritten.


'''

def get_match_folder(match_id):
    return str(match_id)

def get_cache_fname(cache_folder_path, name, match_id):
    if match_id is not None:
        return os.path.join(cache_folder_path,f"{match_id}_{name}.npz")
    return os.path.join(cache_folder_path,f"{name}.npz")


def get_match_fname(cache_folder_path, match_id):
    return get_cache_fname(cache_folder_path, "match", match_id)

def get_team_fname(cache_folder_path, team, match_id):
    return get_cache_fname(cache_folder_path, f"team{team}", match_id)

def get_formations_fname(cache_folder_path, team, match_id, algorithm, threshold):
    fname = f"formations{team}_{algorithm}"
    if algorithm == "legacy":
        fname+=f"_{threshold}"
    return get_cache_fname(cache_folder_path, fname, match_id)

def get_data_match_folder_path(match_folder):
    match_folder = "" if match_folder is None else str(match_folder)
    return os.path.join(DATA_ROOT_FOLDER, match_folder)

def get_cache_folder_path(match_folder):
    return get_data_match_folder_path(match_folder)

def get_tracking_meta_events_fname(match_folder, tracking_fname = None, meta_fname = None, events_fname = None, return_events = True, xml=False):
    match_folder_path = get_data_match_folder_path(match_folder)
    if not os.path.isdir(match_folder_path):
        raise Exception(f"Match folder {match_folder_path} not found.")
    
    if xml:
        tracking_fname = None 
        for f in os.listdir(match_folder_path):
            if f.endswith(".xml"):
                tracking_fname = os.path.join(match_folder_path,f)
        if tracking_fname is None:
            raise Exception("Tracking file not found inside match folder.")
        return tracking_fname, os.path.join(match_folder_path, "meta.json"), None

    if tracking_fname is None or meta_fname is None:
        for f in os.listdir(match_folder_path):
            if tracking_fname is None and f.endswith("tracking.dat"):
                tracking_fname = os.path.join(match_folder_path, f)
            if meta_fname is None and f.endswith("meta.json"):
                meta_fname = os.path.join(match_folder_path, f)
            if events_fname is None and f.endswith("events.json"):
                events_fname = os.path.join(match_folder_path, f)

    if tracking_fname is None:
        raise Exception("Tracking file not found inside match folder.")

    if meta_fname is None:
        raise Exception("Meta file not found inside match folder.")
    
    if not return_events:
        return 
    

    if events_fname is None:
        raise Exception("Events file not found inside match folder.")


    return tracking_fname, meta_fname, events_fname

                
def get_tracking_meta_fname(match_folder, *args, **kwargs):
    return get_tracking_meta_events_fname(match_folder, *args, **kwargs, return_events = False)


def figure_dir(*args, **kwargs):
    return FIGURES_ROOT_FOLDER

def figure_path(fname, *args, **kwargs):
    return os.path.join(figure_dir(*args, **kwargs),fname)
