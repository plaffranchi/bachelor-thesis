import os
import sys
script_dir = os.path.dirname(os.path.realpath(__file__))
local_fname = lambda fname: os.path.join(script_dir, fname)
sys.path.append(os.path.dirname(script_dir))