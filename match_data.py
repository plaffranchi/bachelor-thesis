import csv
import json
import numpy as np
import os
from settings import *
from utils import ProgressBar
import xml_parser
import xml.etree.ElementTree as et




def read_tracking_xml(tracking_fname, meta_fname, verbose):

    match = xml_parser.Match(tracking_fname)

    n_obj = 22
    dt = np.dtype([("frame", np.uint32),("tstamp", np.uint64), ("type_object", np.int8, (n_obj,)), ("no_object",
        np.uint64, (n_obj,)), ("x_object", np.int32, (n_obj,)), ("y_object", np.int32, (n_obj,)), ("ball_x", np.int32), ("ball_y", np.int32), ("possession", np.unicode_, 1), ("alive", np.bool_,1)])

    fps = 25
    tstart, tend = match.frames[0].time.timestamp()*1000, match.frames[-1].time.timestamp()*1000
    ms = 1000/fps
    tstamps = np.arange(tstart, tend+ms, ms)
    l = len(tstamps)
    data = np.zeros(l, dtype=dt)
    data["tstamp"] = tstamps
    data["frame"] = np.arange(l)

    i=0
    for frame in match.frames:
        while data["tstamp"][i] < frame.time.timestamp()*1000:
            i+=1
        data["alive"][i] = int(frame.ballInPlay)
        data["possession"][i] = frame.ballPossession[:1]
        j=0
        for obj in frame.trackingObjs:
            if int(obj.type) != 7:
                data["x_object"][i,j] = obj.x
                data["y_object"][i,j] = obj.y
                data["no_object"][i,j] = obj.id
                data["type_object"][i,j] = obj.type
                j+=1
            else:
                data["ball_x"][i] = obj.x
                data["ball_y"][i] = obj.y
        i+=1

    alive = np.copy(data["alive"])


    md = {}
    md["HomeTeam"] = {}
    md["AwayTeam"] = {}
    md["HomeTeam"]["ShortName"] = "Italy"  # TODO: This is hard coded
    md["AwayTeam"]["ShortName"] = "Spain"  # TODO: This is hard coded
    md["HomeTeam"]["TeamID"] = 66  # TODO: This is hard coded
    md["AwayTeam"]["TeamID"] = 122  # TODO: This is hard coded
    md["FrameRate"] = fps  # TODO: This is hard coded


    md["PitchLongSide"] = match.pitchLength
    md["PitchShortSide"] = match.pitchWidth


    for i, p in zip(range(4), match.phases):
        tstart  = p.start.timestamp()*1000
        md[f"Phase{i+1}StartFrame"] = int(np.argmax(data["tstamp"] >= tstart))
        tend  = p.end.timestamp()*1000
        md[f"Phase{i+1}EndFrame"] =  int(np.argmax(data["tstamp"] >= tend))
        if md[f"Phase{i+1}EndFrame"] == 0:
            md[f"Phase{i+1}EndFrame"] = int(data["frame"][-1])




    with open(meta_fname, "w") as f:
        f.write(json.dumps(md))

    

    return data, alive

            




def read_tracking(tracking_fname, meta_fname, verbose):

    with open(tracking_fname) as f:
        for count, _ in enumerate(f):
            pass

    count+=1
    f = open(tracking_fname)
    reader = csv.reader(f, delimiter=":")

    dt = np.dtype([("frame", np.uint32), ("type_object", np.int8, (29,)), ("no_object",
                np.uint8, (29,)), ("x_object", np.int32, (29,)), ("y_object", np.int32, (29,)), ("ball_x", np.int32), ("ball_y", np.int32), ("possession", np.unicode_, 1), ("alive", np.unicode_,1)])

    data = np.zeros((count), dtype=dt)

    progress_bar = None
    if verbose:
        progress_bar = ProgressBar("Reading tracking file", count, 1)
    

    for i, l in enumerate(reader):
        data["frame"][i] = l[0]
        for j, p in enumerate(l[1].split(";")[:-1]):
            p_data = p.split(",")
            for label, k in zip(["type", "no", "x", "y"], [0,2,3,4]):
                data[f"{label}_object"][i,j] = p_data[k]

        ball = l[2].split(",")
        
        for struct_label, k in zip(["ball_x", "ball_y", "possession", "alive"], [0,1,4,5]):
            data[struct_label][i] = ball[k]

        if progress_bar and progress_bar.is_to_update(i, verbose):
            progress_bar.print()

    if progress_bar:
        progress_bar.terminate()
        
        
    alive = data["alive"] == "A"




    return data, alive


def save_data(cache_folder_path, tracking_fname, meta_fname, match_id, xml, verbose):

    if not xml:
        data, alive = read_tracking(tracking_fname, meta_fname, verbose)
    else:
        data, alive = read_tracking_xml(tracking_fname, meta_fname, verbose)

    ball_position = np.stack([data["ball_x"], data["ball_y"]], axis=1)
    possession = data["possession"] == "H"


    with open(meta_fname) as fp:
        md = json.load(fp)

    phases = []
    iend=0

    frames = data["frame"]
    
    for i in range(4):
        if f"Phase{i+1}StartFrame" in md and md[f"Phase{i+1}EndFrame"]>0:
            kstart  = md[f"Phase{i+1}StartFrame"]
            istart = np.argmax(frames == kstart)
            alive[iend:istart] = False
            kend = md[f"Phase{i+1}EndFrame"]
            iend = np.argmax(frames == kend)
            phases.append((kstart,kend))
    alive[iend:] = False

    l = data["frame"].shape[0]
    match_fname = get_match_fname(cache_folder_path, match_id)

    possession = data["possession"] == "H"

    team_names = [md[f"{t}Team"]["ShortName"] for t in ['Away','Home']]



    np.savez(match_fname, frames=frames, possession=possession, alive=alive, phases=phases, ball_position = ball_position)

    for t in range(2):

        nos = np.unique(data["no_object"][data["type_object"] == t])
        n_active_players = len(nos)
        on_field = np.zeros((l, n_active_players), dtype=np.bool_)
        coords = np.zeros((l, n_active_players, 2), dtype=np.int32)

        team_name = team_names[t]


        progress_bar = None
        if verbose:
            print(f"Saving data for {team_name}")
            progress_bar = ProgressBar("Saving coordinates and on field data", len(nos), 1)

        
        
        for n in nos:
            rows, columns = np.nonzero((data["no_object"] == n) & (data["type_object"] == t)) 
            on_field[rows, nos == n] = True
            coords[rows, nos == n] = np.stack((data["x_object"][rows,columns], data["y_object"][rows,columns]), axis=1)
            if progress_bar:
                progress_bar.print(1)
        
        own_goal_left = np.zeros((l), dtype=np.bool_)

        for phase_kstart, phase_kend in phases:
            istart = np.argmax(phase_kstart == frames)
            iend = np.argmax(phase_kend == frames)
            own_goal_left[istart:iend] = np.mean(coords[istart,on_field[istart],0]) < 0

        coords_team_pov = np.copy(coords)
        coords_team_pov[~own_goal_left] = -coords[~own_goal_left]
        ball_position_team = np.copy(ball_position)
        ball_position_team[~own_goal_left] = -ball_position[~own_goal_left]

        goalkeepers = []

        progress_bar = None
        if verbose:
            progress_bar = ProgressBar("Finding goalkeepers", len(nos), 1)

        for n in nos:
            i_n = np.argmax(nos==n)
            kframes_n_of = on_field[:, i_n]
            means_while_on_field = np.mean(coords_team_pov[kframes_n_of & alive], axis=0)[:,0]

            if np.all(means_while_on_field[i_n] < means_while_on_field[nos != n]):
                goalkeepers.append(n)
            if progress_bar:
                progress_bar.print(1)

    

        if verbose:
            print(f"Goalkeepers for {team_name}: ",",".join([str(g) for g in goalkeepers]))

        team_fname = get_team_fname(cache_folder_path,t, match_id)


        np.savez(team_fname, nos=nos, coords=coords, on_field=on_field, coords_team_pov=coords_team_pov, own_goal_left=own_goal_left, goalkeepers=goalkeepers, ball_position = ball_position_team)





def get_data(match_id, tracking_fname = None, meta_fname = None, cache=True, cache_folder_path = None, xml=False, verbose=False, *args, **kwargs):
    match_folder = get_match_folder(match_id)
    if cache_folder_path is None:
        cache_folder_path = get_cache_folder_path(match_folder)

    match_fname = get_match_fname(cache_folder_path, match_id=match_id)
    team_fname = [get_team_fname(cache_folder_path, team=t, match_id=match_id) for t in range(2)]


    tracking_fname, meta_fname, events_fname = get_tracking_meta_events_fname(match_folder, tracking_fname, meta_fname, xml=xml)

    if not cache or not os.path.isfile(match_fname) or not os.path.isfile(team_fname[0]) or not os.path.isfile(team_fname[1]):
        if verbose and cache:
            print(f"No cache file for selected match {match_id} found.")
        save_data(cache_folder_path, tracking_fname, meta_fname, match_id, xml, verbose)



    match_data = np.load(match_fname)
    teams_data = [np.load(team_fname[t]) for t in range(2)]

    with open(meta_fname) as fp:
        md = json.load(fp)


    goals = [[], []]

    team_ids = [md["AwayTeam"]["TeamID"], md["HomeTeam"]["TeamID"]]
    
    if events_fname is not None:
        with open(events_fname) as fp:
            for line in fp:
                e = json.loads(line)
                if e["event"]=="goal" and e["half_time"]<=4:
                    frame = e["match_run_time_in_ms"] * md["FrameRate"] / 1e3
                    goals[team_ids.index(e["team_id"])].append(frame)

    def multi_index_access(val_away, val_home):
        return {
            0: val_away,
            1: val_home,
            "away_team": val_away,
            "home_team": val_home,
            md["AwayTeam"]["ShortName"]: val_away,
            md["HomeTeam"]["ShortName"]: val_home,
        }


    goals = [np.array(g) for g in goals]
    goals = multi_index_access(*goals)


    out =  multi_index_access(*teams_data) | {
        "team_names": [md["AwayTeam"]["ShortName"], md["HomeTeam"]["ShortName"]],
        "goals": goals,
        "meta": md,
        **match_data,
    }

    def period_and_minute(frame): # frame argument goes from 0 to number of frames
        if not isinstance(frame, np.ndarray):
            if not isinstance(frame, list):
                frame = [frame]
            frame = np.array(frame).astype(np.uint64)
        else:
            frame = np.copy(frame).astype(np.uint64)

        frame += match_data["frames"][0]


    
        phases = match_data["phases"]
        phases = np.resize(phases, (frame.shape[0],phases.shape[0], phases.shape[1]))

        period = np.argmax((frame <= phases[:,:,1].T).T, axis=1) # Period goes from 0 to 1 (or 3, fi extra time)

        minutes_periods = np.array([0,45,90,105])


        minute = (frame-match_data["phases"][period, 0])/md["FrameRate"]/60 + minutes_periods[period]
        if len(period) == 1:
            period = period[0]
            minute = minute[0]


        return period, minute


    out["period_and_minute"] = lambda frame: period_and_minute(frame)

    def get_tid(team):
        if type(team) is str:
            if team.endswith("team"):
                team_id = ["away_team", "home_team"].index(team)
            else:
                team_id = out["team_names"].index(team)
        else:
            team_id = team
        return team_id
    

    out["get_team_id"] = lambda team: get_tid(team)

    return out


if __name__ == "__main__":
    print(get_data(match_id=13, cache=False, verbose=True)["goals"])