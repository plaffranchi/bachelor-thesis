from os.path import join
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mplsoccer import Pitch
from formations import get_formations
from functools import partial
import settings
import datetime 
import sys



class Animation:
    roles_colors = ["blue", "yellow", "grey", "purple", "red"]

    def __init__(self, match_id, team, team_pov=False, *args, **kwargs):


        formations, data = get_formations(match_id=match_id, team=team, *args, **kwargs)

        self.fig, self.ax = plt.subplots()

        self.pitch_x, self.pitch_y = data["meta"]["PitchLongSide"]/100, data["meta"]["PitchShortSide"]/100

        self.fps = data["meta"]["FrameRate"]
        self.edges = formations["edges"]
        self.alive = data["alive"]
        self.on_field = data[team]["on_field"][self.alive]
        self.ball_positions = data[team]["ball_position"][self.alive]
        if team_pov:
            self.coords = data[team]["coords_team_pov"][self.alive]
        else:
            self.coords = data[team]["coords"][self.alive]
        self.nos = data[team]["nos"]
        self.roles = formations["roles"]
        self.codes = formations["codes"]
        self.keyframes = data["keyframes"][self.alive] - data["keyframes"][0]

        self.goalkeepers = data[team]["goalkeepers"]
        self.period_and_minute = data["period_and_minute"]
        self.on_field[:, np.isin(self.nos, self.goalkeepers)] = False
        self.paused = True


        self.fig.canvas.mpl_connect('button_press_event', self.toggle_pause)
        self.fig.canvas.mpl_connect('key_press_event', self.on_key)

        self.init_anim()
    
    def init_anim(self):
        self.draw_pitch()

        self.players_dots =[plt.plot([],[], 'o', color=f"{c}", zorder=2)[0] for c in self.roles_colors]
        self.ball = plt.plot(*self.ball_positions[0], 'o', color=f"green", zorder=2)[0]
        top_left = [self.ax.get_xlim()[0], self.ax.get_ylim()[1]]
        top_right = [self.ax.get_xlim()[1], self.ax.get_ylim()[1]]
        self.kframe_text = plt.text(*top_right,s="", fontdict={"size": "x-large"}, horizontalalignment="right")
        self.periods_text = ["1st", "2nd", "3rd", "4th"]
        self.time_text = plt.text(*top_left,s="", fontdict={"size": "x-large"}, horizontalalignment="left")
        self.code_text = plt.text(0,top_left[1],s="", fontdict={"size": "x-large"}, horizontalalignment="center")

        self.edges_plots = []
        self.players_numbers = {}

        for n in self.nos:
            self.players_numbers[n] = plt.text(0,0,"") 
        


    def draw_pitch(self):
        plt.cla()
        pitch = Pitch(pitch_type='tracab', pitch_length=self.pitch_x, pitch_width=self.pitch_y)
        pitch.draw(ax=self.ax)


    def run(self, i):
        e = self.edges[i]
        of = self.on_field[i]
        code = self.codes[i]
        roles = self.roles[i]
        str_code = "-".join([str(c) for c in code])
        xs, ys = self.coords[i, of].T
        ns = self.nos[of]

        kframe = self.keyframes[i]

        self.kframe_text.set_text(kframe)
        self.code_text.set_text(str_code)


        period, minutes = self.period_and_minute(kframe)

        # print(minutes)
        
        seconds = (minutes * 60) % 60


        # print(minutes, seconds)

        time = f"{int(minutes)}' {int(seconds)}\""
        
        self.time_text.set_text(f"{self.periods_text[period]} {time}")
        n_edges = 0

        self.ball.set_data(*self.ball_positions[i].reshape(2,1))

        for i_n, n in enumerate(ns):
            self.players_numbers[n].set_position((xs[i_n], ys[i_n]))
            self.players_numbers[n].set_text(n)
            neighbours = np.flatnonzero(e[i_n,i_n:])+i_n

            for u in neighbours:
                if n_edges == len(self.edges_plots):
                    edge_plot, = plt.plot([],[], color="black", zorder=1)
                    self.edges_plots.append(edge_plot)
                self.edges_plots[n_edges].set_data(xs[[i_n,u]],ys[[i_n,u]])
                n_edges+=1

        for role, _ in enumerate(self.roles_colors):
            mask = roles[of] == role
            xs_role, ys_role = xs[mask], ys[mask]
            self.players_dots[role].set_data(xs_role,ys_role)

        ns = self.nos[~of]
        for i_n, n in enumerate(ns):
            self.players_numbers[n] = plt.text(0,0,"")
        


        if n_edges < len(self.edges_plots):
            for ep in self.edges_plots[n_edges:]:
                ep.set_data([],[])
        
        self.i = i

        return self.players_dots, self.players_numbers, 


    def get_animation(self, kstart=0, kend=None, speed=1., *args, **kwargs):
        if kend is None:
            kend = len(self.edges)
        self.kstart = kstart
        self.kend = kend
        self.speed = speed
        self.i = 0
        self.animation = animation.FuncAnimation(self.fig, self.run, frames=range(kstart,kend),  interval=1000/self.fps/speed, init_func=self.init_anim)
        return self.animation
    
    def show_animation(self, kstart=0, kend=None, speed=1.):
        self.get_animation(kstart,kend,speed)
        self.toggle_pause()
        plt.show()

    def skip_animation(self, amount):
        self.draw_pitch()
        dur = (self.kend - self.kstart)
        i = (self.i + amount - self.kstart) % dur
        self.animation.frame_seq = self.animation.new_frame_seq()
        for _ in range(i):
            next(self.animation.frame_seq)
        self.init_anim()
        if self.paused:
            self.run(self.kstart + i)
            self.fig.canvas.draw_idle()



    def toggle_pause(self, *args, **kwargs):
        if self.paused:
            self.animation.resume()
        else:
            self.animation.pause()

        self.paused = not self.paused

    def on_key(self, event):
        key = event.key
        short_skip = 1
        long_skip = 1500
        if key==" ":
            self.toggle_pause()
        elif key=="right":
            self.skip_animation(+short_skip)
        elif key=="left":
            self.skip_animation(-short_skip)
        elif key=="up":
            self.skip_animation(+long_skip)
        elif key=="down":
            self.skip_animation(-long_skip)



        

    def plot(self, i):
        self.run(i)
        plt.show()

    def save(self, writer_class, fname, fps=None, *args, **kwargs):
        if fps is None:
            fps = self.fps
        writer = writer_class(fps=fps)
        self.get_animation(*args, **kwargs).save(writer=writer, filename=fname)



if __name__ == "__main__":
    match_id = 13
    team = "Switzerland"
    algorithm = "new"
    ani = Animation(match_id=match_id, team=team, team_pov=True, algorithm=algorithm).show_animation(kstart=1388, kend=None, speed=1)


    # fname = "_".join([str(x) for x in [f"team{team}",algorithm]])
    # f = join(settings.get_data_match_folder_path(match_id),f"{fname}.mp4")

    
    # ani.save(animation.FFMpegWriter, f, kend=5000)


