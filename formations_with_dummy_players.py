from threading import Thread, Lock
from utils import ProgressBar
from scipy.spatial import Delaunay
import numpy as np


thread_lock = Lock()

def compute_formations_thread(codes, roles, process_edges, corners, on_field, coords, process_i, thread_i, length, n_threads, verbose, progress_bar:None|ProgressBar):
        for k in range(thread_i, length, n_threads):
            of = on_field[k]
            pts = np.append(coords[k, of], corners,axis=0)
            n_of = np.sum(of)
            fl = n_of
            fr = fl+1
            bl = fr+1
            br = bl+1

            n_pts = len(pts)

            try:
                tri = Delaunay(pts)
            except:
                codes[k, 0] = -1
                roles[k, of] = -1
                continue

            simp = tri.simplices

            edges = np.zeros((n_pts, n_pts), np.bool_)
            edges[simp, np.roll(simp,-1, axis=1)] = True # Adjacency matrix containing all the edges from the triangulation
            edges = edges | edges.T # Make it simmetric


            lconnected_to_both = np.flatnonzero(edges[fl] & edges[bl])
            lmost_i = np.argmax(pts[lconnected_to_both, 1])
            ml_player = lconnected_to_both[lmost_i]
            
            rconnected_to_both = np.flatnonzero(edges[fr] & edges[br])
            rmost_i = np.argmin(pts[rconnected_to_both, 1])
            mr_player = rconnected_to_both[rmost_i]

            mids = pts[[ml_player,mr_player]]
            mids[0, 1] = corners[0,1]
            mids[1, 1] = corners[1,1]


            ml = br+1
            mr = ml+1

            pts = np.append(pts, mids, axis=0)

            try:
                tri = Delaunay(pts)
            except:
                codes[k, 0] = -1
                roles[k, of] = -1
                continue

            simp = tri.simplices

            polygon_colors = np.zeros(len(simp), np.int16)
            contains_fl = np.any(simp == fl,axis=1)
            contains_fr = np.any(simp == fr,axis=1)
            contains_br = np.any(simp == br,axis=1)
            contains_bl = np.any(simp == bl,axis=1)
            contains_ml = np.any(simp == ml,axis=1)
            contains_mr = np.any(simp == mr,axis=1)

            contains_f = contains_fl | contains_fr
            contains_m = contains_ml | contains_mr
            contains_b = contains_bl | contains_br


            polygon_colors[contains_ml & ~(contains_bl | contains_fl)] = 1 # = left (yellow)
            polygon_colors[~(contains_f | contains_m | contains_b)] = 2 # =  mid (grey)
            polygon_colors[contains_mr & ~(contains_br | contains_fr)] = 3 # = right (purple)
            polygon_colors[~contains_m & contains_f] = 4 # = front (red)
            polygon_colors[(contains_m) & (contains_f | contains_b)] = 5 # = neutral (white)
            # The rest is automatically 0, since we initialized the array with zeros (=  back = blue)

            n_pts = len(pts)
            n_tri = len(simp)

            angles = np.zeros((n_pts,n_pts))
            opposite = np.zeros_like(angles, dtype=np.int16) - 1
            adj_tri = np.copy(opposite)
            edges = np.zeros_like(opposite, np.bool_)



            cycle = np.append(simp, simp[:,0].reshape(simp.shape[0],1), axis=1) # Turn triangles into cycles, by putting the first vertex at the end
            segments = np.linalg.norm(np.diff(pts[cycle],axis=1),axis=2) # Compute the length of each edge of the triangle
            segments_squared = segments ** 2 # Lengths squared
            simp_angles = np.arccos((np.roll(segments_squared,1, axis=1) + np.roll(segments_squared,2, axis=1) - segments_squared) / (2 * np.roll(segments,1, axis=1)*np.roll(segments,2, axis=1))) # Compute the angle of the opposite corner
            next_simp =  np.roll(simp,-1, axis=1)
            angles[simp, next_simp] = simp_angles # Assign vertex tuple to angle (see note above)
            opposite[simp, next_simp] = np.roll(simp,-2, axis=1) # Store the opposite vertex of each edge
            adj_tri[simp, next_simp] = np.mgrid[0:n_tri,0:3][0] # Store to which triangle(s) is the edge adjacent to
            edges[simp, np.roll(simp,-1, axis=1)] = True # Adjacency matrix containing all the edges from the triangulation
            edges = edges | edges.T # Make it simmetric



            stability = np.pi - (angles + angles.T)
            stability[(opposite.T == -1) ^ (opposite == -1)] = np.inf # Edges that only have one oppsite corner are the ones on the convex hull, which have infinite stability

            unstable = np.zeros_like(edges)

            a,b = np.nonzero(np.triu(edges)) # only consider edges "on one direction"
            c = opposite[a,b]
            d = opposite[b,a]

            unstable[a,b] = stability[a,b] < np.min(stability[[a,b,a,b],[c,c,d,d]], axis = 0)

            unstable = unstable.T | unstable # Make it simmetric

            unst_a, unst_b = np.nonzero(unstable)
            poly_ab = polygon_colors[adj_tri[unst_a,unst_b]]
            poly_ba = polygon_colors[adj_tri[unst_b,unst_a]]

            polygon_colors[adj_tri[unst_a,unst_b]] = np.where((poly_ab == 2), poly_ba, poly_ab)

            roles_of = np.zeros(n_of, np.int8) + 2

            front_players = np.unique(simp[polygon_colors == 4])
            # PitchPlot()\
            #     .tripcolor(pts, simp,polygon_colors)\
            #     .plot_edges(pts,edges, stability, unstable, fl, fr, ml, mr, bl, br)\
            #     .plot_points_with_role(pts,roles_of)\
            #     .show()

            front_players = np.delete(front_players, ((front_players == fl) | (front_players == fr)))
            roles_of[front_players] = 4

            back_players = np.unique(simp[polygon_colors == 0])
            back_players = np.delete(back_players, ((back_players == bl) | (back_players == br)))
            roles_of[back_players] = 0

            if polygon_colors[adj_tri[ml_player, ml]] == polygon_colors[adj_tri[ml, ml_player]]: # The color of the polygons adjacent to the edge connecting the dummy left player to the left player found initially is the same
                roles_of[ml_player] = 1

            if polygon_colors[adj_tri[mr_player, mr]] == polygon_colors[adj_tri[mr, mr_player]]: # Same with right side
                roles_of[mr_player] = 3
            
            roles[k, of] = roles_of
            n_of_codes = 5

            for c in range(n_of_codes):
                codes[k, c] = np.sum(roles_of==c)

            edges &= ~unstable
            
            process_edges[k, :n_of, :n_of] = edges[:n_of, :n_of]



            if progress_bar:
                with thread_lock:
                    if progress_bar.is_to_update((k-thread_i)/n_threads, verbose):
                        progress_bar.print()



def compute_formations_process(front, left, coords, on_field, process_i, n_threads, verbose, progress_bar):
    DELTA = 200


    front, left = front+DELTA, left+DELTA
    back, right = -front, -left

    length = coords.shape[0]

    corners = np.array([[front,left], [front,right], [back,left], [back,right]])


    codes = np.zeros((length, 5), dtype=np.int8)
    process_edges = np.zeros((length, 10, 10), dtype=np.bool_)
    roles = np.zeros(on_field.shape, dtype=np.int8) - 1

    kwargs_thread = {
        "codes": codes,
        "roles": roles,
        "process_edges": process_edges,
        "corners": corners,
        "thread_i": 0,
        "length": length,
        "process_i": process_i,
        "verbose": verbose,
        "n_threads": n_threads,
        "on_field": on_field,
        "coords": coords,
        "progress_bar": progress_bar,
    }

    if n_threads>1:
        threads = []

        for j in range(n_threads):
            kwargs_thread["thread_i"] = j
            # chunk = [j::n_threads]
            thread = Thread(target=compute_formations_thread,
                            kwargs=kwargs_thread)
            threads.append(thread)
            thread.start()

        for t in threads:
            t.join()
    else:
        kwargs_thread["n_threads"] = 1
        compute_formations_thread(**kwargs_thread)
    
    return [codes, roles, process_edges]