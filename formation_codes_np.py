import cProfile, pstats, io
from threading import Thread
import multiprocessing as mp
import pandas as pd
from json import load
import numpy as np
from scipy.spatial import Delaunay
from os.path import join, isfile
import time
from settings import *

def get_data(tfp=TRACKING_FILE_PATH, mfp=METADATA_FILE_PATH):
    df = pd.read_csv(tfp, index_col=False, sep=":",
                     names=["keyframe", "29_objects", "ball"])
    df = df.set_index("keyframe")

    with open(mfp) as f:
        md = load(f)

    return df, md


def get_team(df: pd.DataFrame, gk_no, t, cache=True, save=True) -> pd.DataFrame:

    fp = join(DATA_FOLDER, f"team_{t}.csv")

    columns = {
        "type": np.int8,
        "id": np.int8,
        "no": np.int8,
        "x": np.int32,
        "y": np.int32,
        "s": np.float16,
    }

    if cache and isfile(fp):
        return pd.read_csv(fp, index_col="keyframe").sort_values(by=["keyframe", "no"])

    team = pd.DataFrame(columns=list(columns.keys()))

    for i in range(29):
        d = df["29_objects"].str.split(";").str[i].str.split(",").str
        o = pd.DataFrame(columns=list(columns.keys()))

        for j, dtype in enumerate(columns.values()):
            o[o.columns[j]] = d[j].astype(dtype)

        in_team = o[(o["type"] == t) & (o["no"] != gk_no)]

        team = pd.concat((team, in_team))

    team.index.rename('keyframe', inplace=True)
    team = team.sort_values(by=["keyframe", "no"])

    if save:
        team.to_csv(fp, index_label="keyframe")

    return team


def get_possession(df: pd.DataFrame, t):
    t_letter = "H" if t else "A"
    return (df["ball"].str.split(",").str[4] == t_letter).to_numpy()

def get_alive(df: pd.DataFrame):
    return (df["ball"].str.split(",").str[5].str.removesuffix(";") == "Alive").to_numpy()


def get_team_np(df: pd.DataFrame, team: pd.DataFrame, md, t, cache=True):
    team_fname = join(DATA_FOLDER, f"team_{t}.npz")
    # speeds_fname = join(DATA_FOLDER, f"speeds_{t}.npy")


    if cache and isfile(team_fname):
        return np.load(team_fname)

    # segments = []
    phases = []
    alive = get_alive(df)

    keyframes = df.index.to_numpy()

    iend=0

    for i in range(4):
        if f"Phase{i+1}StartFrame" in md:
            # segments.append(
                # np.arange(start=md[f"Phase{i+1}StartFrame"], stop=md[f"Phase{i+1}EndFrame"]+1))
            kstart  = md[f"Phase{i+1}StartFrame"]
            istart = np.argmax(keyframes == kstart)
            alive[iend:istart] = False
            kend = md[f"Phase{i+1}EndFrame"]
            iend = np.argmax(keyframes == kend)
            phases.append((kstart,kend))

    alive[iend:] = False

    nos = np.sort(team["no"].unique())
    tot_kfs = keyframes.shape[0]
    coords = np.zeros((tot_kfs, nos.shape[0], 2), dtype=np.int32)
    on_field = np.zeros((tot_kfs, nos.shape[0]), dtype=np.bool_)
    speeds = np.zeros((tot_kfs, nos.shape[0]), dtype=np.float32)


    possession = get_possession(df, t)

    for no in nos:
        t_no = team.loc[(team["no"] == no)]
        
        kns = t_no.index.to_numpy()
        ixs = np.isin(keyframes, kns)

        coords[ixs, nos == no] = t_no[["x", "y"]]
        speeds[ixs, nos == no] = t_no["s"]
        on_field[ixs, nos == no] = True

    np.savez(team_fname, keyframes=keyframes, nos=nos, coords=coords,
             speeds=speeds, on_field=on_field, possession=possession, alive=alive, phases=phases)

    return np.load(team_fname)


def compute_formations(coords, on_field, threshold, i, verbose=False):
    l = coords.shape[0]

    codes = np.zeros((l, 5), dtype=np.int8)
    process_edges = np.zeros((l, 10, 10), dtype=np.int8)

    n_players = coords.shape[1]

    n_threads = 2

    roles = np.zeros(on_field.shape)

    def compute_formations_thread(j):
        ts = time.time()
        # l = len(keyframes_ns)

        # print("Entered", i*n_threads+j, keyframes_ns[0], "...",
        #       keyframes_ns[-1], ",", j, (l-1) * n_threads + j, f"({len(keyframes_ns)})")

        for i_e in range(j, l, n_threads):
            edges = np.zeros((10, 10), dtype=np.int16)

            of = on_field[i_e]

            pts = coords[i_e, of]


            try:
                dnay = Delaunay(pts)
            except:
                if verbose:
                    print("Process",i,"thread",j,"Unable to perform D. triangulation on points", pts)
                continue

            def set_edge(a, b, c):
                if a < b:
                    if edges[a, b] > 0:
                        edges[b, a] = edges[a, b]
                    edges[a, b] = c+1
                elif edges[b, a] > 0:
                    edges[a, b] = c+1
                else:
                    edges[b, a] = c+1

            for (a, b, c) in dnay.simplices:
                set_edge(a, b, c)
                set_edge(c, a, b)
                set_edge(b, c, a)

            edges_ixs = np.array(np.nonzero(edges))
            triag_edges_ixs = edges_ixs[:, edges_ixs[1] > edges_ixs[0]]

            edges_2_opp_ixs_mask = edges[triag_edges_ixs[1],
                                         triag_edges_ixs[0]] != 0

            edges_2_opp_ixs = triag_edges_ixs[:, edges_2_opp_ixs_mask]
            edges_1_opp_ixs = triag_edges_ixs[:, ~edges_2_opp_ixs_mask]

            # Remove edge at the borders

            p, q = edges_1_opp_ixs
            a = edges[p, q] - 1

            u, v, u_ = pts[[p, q, a]]

            uv = v - u
            u_u = u - u_

            m = np.stack([uv, u_u], axis=1)

            det = np.linalg.det(m)
            delta = np.abs(det) / (2*np.linalg.norm(uv, axis=1))

            mask = delta > threshold

            edges[p, q] = mask

            # Remove edges with two opposite vertices

            p, q = edges_2_opp_ixs
            a, b = edges[p, q] - 1, edges[q, p] - 1

            u_, v_, u, v = pts[[b, a, p, q]]

            uv = v - u
            s = (u + v)/2
            u_v_ = v_ - u_

            inv_mat = np.linalg.inv(np.stack([np.stack([-uv[:, 1], -u_v_[:, 1]], axis=1),
                                              np.stack([uv[:, 0], u_v_[:, 0]], axis=1)], axis=1))[:, 0]

            t = (u_ + v_ - u - v)/2

            lamb = inv_mat[:, 0] * t[:, 0] + inv_mat[:, 1] * t[:, 1]

            lamb = np.stack([lamb, lamb], axis=1)

            c = c = lamb * np.stack([-uv[:, 1], uv[:, 0]], axis=1) + s

            d_vec_norm = np.linalg.norm(u - c, axis=1)
            D_vec_norm = np.linalg.norm(u_ - c, axis=1)

            delta = (D_vec_norm - d_vec_norm)/2

            mask = delta > threshold

            edges[p, q] = mask

            process_edges[i_e] = edges

            edges_ixs = np.array(np.nonzero(edges))
            triag_edges_ixs = edges_ixs[:, edges_ixs[1] > edges_ixs[0]]

            angles = np.arctan2(pts[triag_edges_ixs[1], 1] - pts[triag_edges_ixs[0], 1],
                                pts[triag_edges_ixs[1], 0] - pts[triag_edges_ixs[0], 0]) + np.pi
            angles_opp = (angles + np.pi) % (2*np.pi)

            angles = np.concatenate((angles, angles_opp))

            triag_edges_ixs_opp = np.array(
                [triag_edges_ixs[1], triag_edges_ixs[0]])
            ixs = np.concatenate(
                (triag_edges_ixs, triag_edges_ixs_opp), axis=1)

            def sorted_neighs(v, shift_angle):
                shift_angle %= 2*np.pi
                a = ((angles - shift_angle + np.pi) % (2*np.pi))
                ab_order = np.argsort(a)
                ixs_ = ixs.T[ab_order]
                return ixs_[ixs_[:, 0] == v, 1]

            def next_edge(v, u, d):
                neighs = sorted_neighs(v, 0)
                edge = np.argwhere(neighs == u)
                if not len(edge):
                    return -1
                i = edge[0, 0]
                return neighs[(i+d) % len(neighs)]

            def navigate(v, v_, up, arr, d):
                arr[v] = True
                def compare(a, b): return a < b if up else a > b
                while compare(pts[v, 1], pts[v_, 1]):
                    v, v_ = v_, next_edge(v_, v, d)
                    arr[v] = True
                return v

            def get_tb(func):
                return np.flatnonzero(func(pts[:, 0]) == pts[:, 0])

            pos = roles[i_e, of]

            backline = np.zeros(pos.shape[0], dtype=np.bool_)
            frontline = np.zeros(pos.shape[0], dtype=np.bool_)

            bottoms = get_tb(np.amin)
            lmost_back = lmost_front = np.argmin(pts[:, 1]) # Take the rightmost to make sure it gets updated
            rmost_back = rmost_front = np.argmax(pts[:, 1]) # Take the leftmost to make sure it gets updated

            undef_code = False

            for bottom in bottoms:
                v = bottom
                n_bottom = sorted_neighs(v, -np.pi/2)

                if not len(n_bottom):
                    undef_code = True
                    break

                v_ = n_bottom[-1]

                lmost_back_ = navigate(v, v_, True, backline, -1)
                if pts[lmost_back_,1] > pts[lmost_back,1]:
                    lmost_back = lmost_back_


                v = bottom
                v_ = n_bottom[0]
                rmost_back_ = navigate(v, v_, False, backline, 1)
                if pts[rmost_back_,1] < pts[rmost_back,1]:
                    rmost_back = rmost_back_

            if not undef_code:
                tops = get_tb(np.amax)

                for top in tops:
                    v = top
                    n_top = sorted_neighs(v, np.pi/2)

                    if not len(n_top):
                        undef_code = True
                        break

                    v_ = n_top[-1]
                    rmost_front_ = navigate(v, v_, False, frontline, -1)
                    if pts[rmost_front_,1] < pts[rmost_front,1]:
                        rmost_front = rmost_front_


                    v = top
                    v_ = n_top[0]
                    lmost_front_ = navigate(v, v_, True, frontline, 1)
                    if pts[lmost_front_,1] > pts[lmost_front,1]:
                        lmost_front = lmost_front_
            if undef_code:
                codes[i_e, 0] = -1
                roles[i_e, of] = -1
                continue


            pos[(~frontline) & (~backline)] = 3
            pos[backline] = 1
            pos[frontline] = 5
            if lmost_back == lmost_front:
                pos[lmost_back] = 2

            if rmost_back == rmost_front:
                pos[rmost_back] = 4

            for c_i in range(5):
                codes[i_e, c_i] = (pos == c_i+1).sum()

            roles[i_e, of] = pos

        if verbose:
            print("Done", i*n_threads+j, "elapsed", time.time()-ts)

    threads = []

    for j in range(n_threads):

        # chunk = [j::n_threads]
        thread = Thread(target=compute_formations_thread,
                        args=(j,))
        threads.append(thread)
        thread.start()

    for t in threads:
        t.join()

    return [codes, roles, process_edges]


def get_codes_pos_nos(threshold, t, cache=True, cache_team_np=True, cache_team_csv=True, verbose=False):


    fname = join(DATA_FOLDER, f"team_{t}_{threshold}.npz")

    df, md = get_data()

    gk_no = GK_NO(t)
    team: pd.DataFrame = get_team(df, gk_no=gk_no, t=t, cache=cache_team_csv)

    team_np = get_team_np(df, team, md, t=t, cache=cache_team_np)

    # return team_np

    if isfile(fname) and cache:
        return np.load(fname), team_np

    tot_kfs = team_np["alive"].sum()

    n_processes = mp.cpu_count()

    step = (tot_kfs//n_processes)

    reminder = tot_kfs % n_processes

    splits = np.arange(start=step, stop=tot_kfs-reminder, step=step)

    data = {}

    for f in team_np.files:
        if f not in ["nos", "alive", "phases"]:
            data[f] = np.split(team_np[f][team_np["alive"]], splits)

    ts = time.time()

    codes = np.zeros((tot_kfs, 6), dtype=np.int16)
    pos = np.zeros(team_np["on_field"][team_np["alive"]].shape, dtype=np.int32)
    edges = np.zeros((tot_kfs, 10, 10), dtype=np.int32)



    p = mp.Pool(n_processes)
    result = p.starmap(compute_formations, zip(data["coords"], data["on_field"], [
                       threshold]*n_processes, [i for i in range(n_processes)], [verbose for _ in range(n_processes)]))

    p.close()
    p.join()

    starts = list(splits)
    starts.insert(0, 0)
    ends = list(splits)
    ends.append(tot_kfs)

    for i, (s, e) in enumerate(zip(starts, ends)):
        codes[s:e, :5] = result[i][0]
        pos[s:e] = result[i][1]
        edges[s:e] = result[i][2]

    codes[:, 5] = team_np["possession"][team_np["alive"]]

    if verbose:
        print(codes)
        print(pos)
        print(edges)

        print("Number of undefined codes:", (codes[:, 0] == -1).sum())

        print("Elapsed", time.time()-ts)

    np.savez(fname, codes=codes, pos=pos, edges=edges)

    return np.load(fname), team_np


if __name__ == "__main__":
    pr = cProfile.Profile()
    pr.enable()
    get_codes_pos_nos(threshold=150, t=1, cache=False, cache_team_np=False, cache_team_csv=False, verbose=True)
    pr.disable()
    pr.dump_stats("generate_codes.prof")
    

