from threading import Thread, Lock
from utils import ProgressBar
from scipy.spatial import Delaunay
import numpy as np


thread_lock = Lock()


def compute_formation(pts):
    n_of = len(pts)
    if n_of > 10:
        edges = np.zeros((10, 10), np.bool_)
        return np.array([-1, 0, 0, 0, 0]), np.zeros(n_of)-1, edges, np.zeros_like(edges), np.zeros_like(edges, np.float32), np.zeros_like(edges), np.zeros_like(edges), np.zeros((10, 10, 2))

    stability = np.zeros((n_of, n_of), dtype=np.float64)

    dnay = Delaunay(pts)

    simp = (dnay.simplices)

    # `opposite` is a matrix that contains the vertices that are opposite to the edge.
    # For each counter-clockwise directed edge (P,Q) on the convex hull, the entry (P,Q) contains the index of the vertex in the same triangle, while the entry (Q,P)
    # is -1. For edges inside a quadrangle we can get the two other vertices on the quadrangle by taking the value of (P,Q) and (Q,P).
    # To summarize, if (P,Q) is counterclockwise oriented, the entry (P,Q) in the matrix contains the vertex which is left to the edge,
    # and all other entries are -1.

    opposite = np.zeros_like(stability, dtype=np.int16) - 1
    next_simp = np.roll(simp, -1, axis=1)
    # Store the opposite vertex of each edge
    opposite[simp, next_simp] = np.roll(simp, -2, axis=1)

    # If opposite is not -1, there is an edge connecting the two vertices
    edges = opposite != -1
    triag_edges = np.copy(edges)

    unstable_inner_edges = np.zeros_like(edges)

    # Outer, i.e. edges that lie on the convex-hull/perimeter, are the ones which have an opposite vertex only in one direction.
    outer = edges & (edges ^ edges.T)
    outer_i = np.nonzero(outer)

    # Stability for edges on the convex hull:

    p, q = outer_i
    a = opposite[p, q]

    u, v, u_ = pts[[p, q, a], :]

    uv = v - u
    u_u = u - u_

    m = np.stack([uv, u_u], axis=1)

    det = np.linalg.det(m)
    delta = np.abs(det) / (2*np.linalg.norm(uv, axis=1))

    stability[p, q] = delta

    # Stability for edges inside the convex hull:

    inner_i = np.nonzero(np.triu(edges) & ~outer)

    p, q = inner_i

    a, b = opposite[p, q], opposite[q, p]

    u_, v_, u, v = pts[[b, a, p, q], :]

    uv = v - u
    s = (u + v)/2
    u_v_ = v_ - u_

    inv_mat = np.linalg.inv(np.stack([np.stack(
        [-uv[:, 1], -u_v_[:, 1]], axis=1), np.stack([uv[:, 0], u_v_[:, 0]], axis=1)], axis=1))[:, 0]

    t = (u_ + v_ - u - v)/2

    lamb = inv_mat[:, 0] * t[:, 0] + inv_mat[:, 1] * t[:, 1]

    lamb = np.stack([lamb, lamb], axis=1)

    c = lamb * np.stack([-uv[:, 1], uv[:, 0]], axis=1) + s

    d_vec_norm = np.linalg.norm(u - c, axis=1)
    D_vec_norm = np.linalg.norm(u_ - c, axis=1)

    delta = (D_vec_norm - d_vec_norm)/2
    stability[p, q] = delta

    # This commented part was for angle stability, but I use distance here:

    # angles = np.zeros((n_of,n_of))
    # cycle = np.append(simp, simp[:,0].reshape(simp.shape[0],1), axis=1) # Turn triangles into cycles, by putting the first vertex at the end
    # segments = np.linalg.norm(np.diff(pts[cycle],axis=1),axis=2) # Compute the length of each edge of the triangle
    # segments_squared = segments ** 2 # Lengths squared
    # simp_angles = np.arccos((np.roll(segments_squared,1, axis=1) + np.roll(segments_squared,2, axis=1) - segments_squared) / (2 * np.roll(segments,1, axis=1)*np.roll(segments,2, axis=1))) # Compute the angle of the opposite corner
    # angles[simp, next_simp] = simp_angles # Assign vertex tuple to angle (see note above)
    # stability = np.pi - (angles + angles.T)

    # Make stabilty simmetric (probabily not required)
    stability[stability.T > 0] = stability.T[stability.T > 0]

    removed = True
    while removed:
        outer = edges & (edges ^ edges.T)
        outer_i = np.nonzero(outer)

        p, q = outer_i

        sort_pq = np.argsort(stability[p, q])
        p, q = p[sort_pq], q[sort_pq]

        # Remove if stability smaller than all the edges on the triangle:

        a = opposite[p, q]
        unstable = stability[p, q] < np.min(stability[[a, q], [p, a]], axis=0)

        if not np.any(unstable):
            break

        p_q = np.append(p, q)
        removed = False
        # Make sure that the other vertex does not lie on the perimeter.
        a_not_on_perimeter = ~np.isin(a[unstable], p_q)

        if np.any(a_not_on_perimeter):
            i = np.argmax(a_not_on_perimeter)
            p_, q_, a_ = p[unstable][i], q[unstable][i], a[unstable][i]
            edges[p_, q_] = False  # Remove edge on the perimeter
            # The other two edges are now on the perimeter, so their edge exists "only in one direction"
            edges[a_, p_] = False
            edges[q_, a_] = False

            # Update the stability of the two edges
            b = opposite[[p_, a_], [a_, q_]]

            u, v, u_ = pts[[[a_, q_], [p_, a_], b], :]

            uv = v - u
            u_u = u - u_

            m = np.stack([uv, u_u], axis=1)

            det = np.linalg.det(m)
            delta = np.abs(det) / (2*np.linalg.norm(uv, axis=1))

            stability[[p_, a_], [a_, q_]] = delta
            stability[[a_, q_], [p_, a_]] = delta
            removed = True

    inner = edges & ~outer
    assert np.all(inner.T == inner)

    # Remove all edges inside the perimeter (inside quadrangles)
    inner_i = np.nonzero(inner)
    p, q = inner_i
    a, b = opposite[p, q], opposite[q, p]
    min_quad_stability = np.min(stability[[p, b, a, q], [a, p, q, b]], axis=0)
    inner_stable = stability[p, q] >= min_quad_stability
    edges[p, q] = inner_stable
    inner_unstable = ~inner_stable
    p_unst, q_unst = p[inner_unstable], q[inner_unstable]
    unstable_inner_edges[p_unst, q_unst] = True

    centers = np.zeros((n_of, n_of, 2), np.float64)
    tri = simp.T
    centers[tri, np.roll(tri, -1, axis=0)] = np.mean(pts[tri], axis=0)

    r, s = np.nonzero(unstable_inner_edges)
    p, q = opposite[[r, s], [s, r]]
    quadrangle = [p, s, q, r]
    a, b = quadrangle, np.roll(quadrangle, -1, axis=0)
    o = np.mean(pts[quadrangle, :].astype(np.float64), axis=0)
    centers[a, b] = o
    centers[b, a] = o

    p, q = outer_i
    o = centers[p, q]

    op = pts[p] - o
    oq = pts[q] - o

    prod = op * oq

    same_side = prod[:, 0] > 0
    # on_side = prod[:, 1] > 0
    # same_side &= ~on_side
    front = same_side & (op[:, 0] > 0)
    back = same_side & (op[:, 0] < 0)
    front |= (op[:, 1] < 0) & (oq[:, 1] > 0)
    back |= (op[:, 1] > 0) & (oq[:, 1] < 0)

    outer_adj_list_cckwise = np.zeros(n_of, np.int8) - 1
    outer_adj_list_cckwise[p] = q

    i_start = np.argmin(pts[p, 0])
    p_start = p[i_start]
    q_ = outer_adj_list_cckwise[p_start]
    cckwise = np.zeros_like(p)
    cckwise[0] = i_start
    i = 1
    while q_ != p_start:
        cckwise[i] = np.argmax(p == q_)
        q_ = outer_adj_list_cckwise[q_]
        i += 1

    p = (p[cckwise])
    q = (q[cckwise])
    front = (front[cckwise])
    back = (back[cckwise])

    top_player = np.argmax(pts[p, 0])

    l = len(front)
    roles_of = np.zeros(n_of, np.int8) + 2

    if not np.any(back):
        first_back = last_back = l-top_player
    else:
        back_from_top = np.roll(back, -top_player)
        first_back = np.argmax(back_from_top)
        last_back = l - np.argmax(back_from_top[::-1])
        back_from_top[first_back:last_back] = True
        back = np.roll(back_from_top, top_player)

    if not np.any(front):
        first_front = last_front = top_player
    else:
        first_front = np.argmax(front)
        last_front = l - np.argmax(front[::-1])
        front[first_front:last_front] = True

    both = front & back
    front[both] = False
    back[both] = False

    if np.any(back):
        back_from_top = np.roll(back, -top_player)
        first_back = np.argmax(back_from_top)
        last_back = (l - np.argmax(back_from_top[::-1]))
        p_from_top = np.roll(p, -top_player)
        roles_of[p_from_top[first_back:last_back+1]] = 0

    if np.any(front):
        first_front = (np.argmax(front))
        last_front = (l - np.argmax(front[::-1]))
        roles_of[p[first_front:last_front+1]] = 4

    first_back = (first_back + top_player) % l
    last_back = (last_back + top_player) % l
    if first_back == 0:
        first_back = l

    if last_front == first_back:
        lf = last_front % l
        roles_of[p[lf]] = 1
    elif first_back - last_front > 1:
        lefts = p[last_front+1:first_back]
        lmost = np.argmax(pts[lefts, 1])
        roles_of[lefts[lmost]] = 1
        # roles_of[lefts] = 1

    if first_front == last_back:
        roles_of[p[last_back]] = 3
    elif first_front - last_back > 1:
        rights = p[last_back+1:first_front]
        rmost = np.argmin(pts[rights, 1])
        roles_of[rights[rmost]] = 3
        # roles_of[rights] = 3

    roles_of[p[0]] = 0
    roles_of[p[top_player]] = 4

    n_of_codes = 5

    code = np.zeros(5, np.int8)

    for c in range(n_of_codes):
        code[c] = np.sum(roles_of == c)

    front_ = np.zeros_like(edges)
    back_ = np.zeros_like(edges)
    front_[p[front], q[front]] = True
    back_[p[back], q[back]] = True

    return code, roles_of, edges, triag_edges, stability, front_, back_, centers


def compute_formations_thread(codes, roles, process_edges, on_field, coords, process_i, thread_i, length, n_threads, verbose, progress_bar: None | ProgressBar):
    for k in range(thread_i, length, n_threads):
        of = on_field[k]
        pts = coords[k, of]
        code, rol, edg, _, _, _, _, _ = compute_formation(pts)
        codes[k] = code
        roles[k, of] = rol
        process_edges[k] = edg.T | edg
        if progress_bar:
            with thread_lock:
                if progress_bar.is_to_update((k-thread_i)/n_threads, verbose):
                    progress_bar.print()


def compute_formations_process(coords, on_field, process_i, n_threads, verbose, progress_bar):

    length = coords.shape[0]
    codes = np.zeros((length, 5), dtype=np.int8)
    process_edges = np.zeros((length, 10, 10), dtype=np.bool_)
    roles = np.zeros(on_field.shape, dtype=np.int8) - 1

    kwargs_thread = {
        "codes": codes,
        "roles": roles,
        "process_edges": process_edges,
        "thread_i": 0,
        "length": length,
        "process_i": process_i,
        "verbose": verbose,
        "n_threads": n_threads,
        "on_field": on_field,
        "coords": coords,
        "progress_bar": progress_bar,
    }

    if n_threads > 1:
        threads = []

        for j in range(n_threads):
            kwargs_thread["thread_i"] = j
            # chunk = [j::n_threads]
            thread = Thread(target=compute_formations_thread,
                            kwargs=kwargs_thread)
            threads.append(thread)
            thread.start()

        for t in threads:
            t.join()
    else:
        kwargs_thread["n_threads"] = 1
        compute_formations_thread(**kwargs_thread)

    return [codes, roles, process_edges]
