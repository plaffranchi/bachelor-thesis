from utils import ProgressBar

from multiprocessing.managers import BaseManager
import multiprocessing as mp
import numpy as np
from match_data import get_data
from utils import ProgressBar
import os
from settings import *
import time



def get_formations(match_id, team, algorithm: str = "barycenter", threshold=150, n_processes = mp.cpu_count() , n_threads = 2, cache=True, cache_team=True, verbose=False, *args, **kwargs):


    match_folder = get_match_folder(match_id)
    data = get_data(match_folder, cache=cache_team, verbose=verbose, *args, **kwargs)


    if not (algorithm=="dummy" or algorithm=="legacy" or algorithm=="barycenter"):
        raise Exception(f"Invalid algorithm argument: {algorithm}")


    team_id = data["get_team_id"](team)

    cache_folder_path = get_cache_folder_path(match_folder)
    formations_fname = get_formations_fname(cache_folder_path, team_id, match_id = match_id, algorithm=algorithm, threshold=threshold)

    if cache and os.path.isfile(formations_fname):
        if verbose:
            print("Formations' cache file found.")
        return np.load(formations_fname), data
    
    if verbose and cache:
        print("Formations' cache file not found.")

    tot_kfs = data["alive"].sum()

    team_np = data[team]

    
    step = (tot_kfs//n_processes)
    reminder = tot_kfs % n_processes
    splits = np.arange(start=step, stop=tot_kfs-reminder, step=step)

    split = lambda arr: np.split(arr[data["alive"]], splits)
    coords_team_pov = split(team_np["coords_team_pov"])
    on_field = np.copy(team_np["on_field"])
    tot_on_field = on_field.shape[1]
    # Remove goalkeeper from the players on field
    on_field[:, np.isin(team_np["nos"], team_np["goalkeepers"])] = False 



    on_field = split(on_field)


    codes = np.zeros((tot_kfs, 6), dtype=np.int8)
    roles = np.zeros((tot_kfs, tot_on_field), dtype=np.int8) - 1
    edges = np.zeros((tot_kfs, 10, 10), dtype=np.bool_)

    front = data["meta"]["PitchLongSide"]/2
    left =  data["meta"]["PitchShortSide"]/2
    



    p = mp.Pool(n_processes)
    progress_bar = None
    if verbose:
        BaseManager.register("ProgressBar", ProgressBar)
        manager = BaseManager()
        manager.start()
        team_name = data["team_names"][team_id]
        progress_bar = manager.ProgressBar(f"Computing formations for {team_name}", tot_kfs, n_threads*n_processes)


    if algorithm == "dummy":
        from formations_with_dummy_players import compute_formations_process        
        args = zip(
            [front for _ in range(n_processes)],
            [left for _ in range(n_processes)],
            coords_team_pov, 
            on_field,
            [i for i in range(n_processes)],
            [n_threads for _ in range(n_processes)],
            [verbose for _ in range(n_processes)],
            [progress_bar for _ in range(n_processes)],
        )
    elif algorithm=="legacy":
        from formations_with_threshold import compute_formations_process
        args = zip(
            coords_team_pov, 
            on_field,
            [threshold for _ in range(n_processes)],
            [i for i in range(n_processes)],
            [n_threads for _ in range(n_processes)],
            [verbose for _ in range(n_processes)],
            [progress_bar for _ in range(n_processes)],
        )
    else:
        from formation_barycenter import compute_formations_process        
        args = zip(
            coords_team_pov, 
            on_field,
            [i for i in range(n_processes)],
            [n_threads for _ in range(n_processes)],
            [verbose for _ in range(n_processes)],
            [progress_bar for _ in range(n_processes)],
        )



    ts = time.time()

    result = p.starmap(compute_formations_process, args)

    p.close()
    p.join()

    te = time.time()

    if progress_bar:
        progress_bar.terminate()


    starts = list(splits)
    starts.insert(0, 0)
    ends = list(splits)
    ends.append(tot_kfs)

    for i, (s, e) in enumerate(zip(starts, ends)):
        codes[s:e, :5] = result[i][0]
        roles[s:e] = result[i][1]
        edges[s:e] = result[i][2]

    codes[:, 5] = data["possession"][data["alive"]] == team_id

    # if verbose:
    print("Number of undefined codes:", (codes[:, 0] == -1).sum())

    print("Elapsed", te-ts)

    np.savez(formations_fname, codes=codes, roles=roles, edges=edges)

    return np.load(formations_fname), data


if __name__ == "__main__":
    # match_id = "ita_spa"
    match_id = 13
    # get_formations(match_id=match_id, algorithm="barycenter", n_processes=8, n_threads=2, team=1, cache=False, cache_team=True, verbose=False)
    print(len(get_data(match_id)["alive"]))
    
    # get_formations(match_id=match_id, algorithm="barycenter",  team=1, cache=False, cache_team=True, verbose=False)
    # get_formations(match_id=match_id, algorithm="barycenter",  team="Spain", cache=False, cache_team=False, xml=True, verbose=True)

    

