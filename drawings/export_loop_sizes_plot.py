import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from add_parent_dir import *
from network_processing import TeamNet, BothTeamCartesianNet
from settings import figure_path

# mpl.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'font.size' : 11,
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })


net = TeamNet(56, "Switzerland")
net_elab = net.collapse_loops().add_zero_loops()

dead_loops_sizes, dead_loops_sizes_distr = net_elab.dead_loop_sizes.distr
alive_loops_sizes, alive_loops_sizes_distr = net_elab.alive_loop_sizes.distr

loops_sizes, loops_sizes_distr = net_elab.loop_sizes.distr
mean = np.mean(net_elab.loop_sizes)
print("Mean team net", mean)
max_loop_size = loops_sizes[-1]
loops_distr_log = np.log(loops_sizes_distr)

x = np.arange(max_loop_size+1)

d = np.zeros_like(x)
a = np.zeros_like(d)

d[dead_loops_sizes] = dead_loops_sizes_distr
a[alive_loops_sizes] = alive_loops_sizes_distr

fig, axes = plt.subplots(2, 1, figsize=(5,12), dpi=300)
(ax1, ax2) = axes
for ax in axes:
    ax.set_yscale("log")
    ax.set_xlim([-1,200])
    ax.set_ylabel("Number of loops [log]")

ax1.bar(x+d, a+d, bottom=0, label="Alive loops", color="tab:blue")
ax1.bar(x, d, label="Dead loops", color="tab:red")

# ax1.axvline(median_log, color="red", label="Mean log loop size")
ax1.axvline(mean, color="black", label="Mean loop duration")
# ax1.plot(exp, color="tab:orange", label=f"Exponential curve fitted on first {sample_fit} samples")
ax1.legend()
ax1.set_title(net.team_name)

net = BothTeamCartesianNet(56)
net_elab = net.collapse_loops().add_zero_loops()
dead_loops_sizes, dead_loops_sizes_distr = net_elab.dead_loop_sizes.distr
alive_loops_sizes, alive_loops_sizes_distr = net_elab.alive_loop_sizes.distr

loops_sizes, loops_sizes_distr = net_elab.loop_sizes.distr
mean = np.mean(net_elab.loop_sizes)
print("Mean Cartesian net", mean)
max_loop_size = loops_sizes[-1]
loops_distr_log = np.log(loops_sizes_distr)

x = np.arange(max_loop_size+1)

d = np.zeros_like(x)
a = np.zeros_like(d)

d[dead_loops_sizes] = dead_loops_sizes_distr
a[alive_loops_sizes] = alive_loops_sizes_distr

ax2.axvline(mean, color="black", label="Mean loop size")
ax2.bar(x+d, a+d, bottom=0, label="Alive loops", color="tab:blue")
ax2.bar(x, d, label="Dead loops", color="tab:red")


# fig.tight_layout(h_pad=2, w_pad=5)

# ax2.set_xlabel("$d_{tot}$ [nfr]")
ax1.set_xlabel("Duration [nfr]")
ax2.set_xlabel("Duration [nfr]")
ax2.set_title("Cartesian net")


plt.subplots_adjust(hspace=.4)
fig.savefig(figure_path("loop_sizes.png"), bbox_inches='tight')
# plt.show()