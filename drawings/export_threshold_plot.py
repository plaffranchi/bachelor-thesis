from add_parent_dir import *
import network_processing
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from settings import figure_path


# mpl.use("pgf")
# mpl.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'font.size' : 11,
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })


match_id = 56
team_id = 0

net_elab = network_processing.TeamNet(match_id=match_id, algorithm="barycenter", collapse_dead_edges=True, team=team_id).collapse_loops()
carnet_elab = network_processing.BothTeamCartesianNet(match_id=match_id, algorithm="barycenter").collapse_loops()

t_min_max = [
    [0, 100],
    [0, 100],
]

data = []

for i, net in enumerate([net_elab, carnet_elab]):
    n_nodes = []
    n_edges = []
    n_edges_deg = []
    coords = []
    sizes = []
    resulting_nodes = []
    t_max = t_min_max[i][1]
    thresholds = np.arange(0,t_max)


    init_n = net.n_nodes
    for j, t_loop in enumerate(thresholds):
        net_collapse_loops = net.remove_loops(t_loop, verbose=False).collapse_loops(verbose=False)
        n_edges.append(net_collapse_loops.n_edges)
        nodes, degs = net_collapse_loops.degree

        n = net_collapse_loops.n_nodes
        assert len(degs) == n

        degs, deg_counts = np.unique(degs, return_counts=True)


        for d, d_count in zip(degs, deg_counts):
            nd = 0
            ne = 0
            # if d < degs[-1]:
            #     net_collapse_deg = net_collapse_loops.remove_small_degree(d,verbose=False, iterate=False)
            #     nd = net_collapse_deg.n_nodes
            #     ne = net_collapse_deg.n_edges
            # resulting_nodes.append(nd)
            n_edges_deg.append(ne)
            coords.append([t_loop,d])
            sizes.append(d_count)
            n_nodes.append(n)
                

        # resulting_nodes.append(n)
        # coords.append([t_loop,0])
        # sizes.append(init_n - n)
        # n_nodes.append(n)
    data.append([n_nodes.copy(), n_edges.copy(), n_edges_deg.copy(), coords.copy(), sizes.copy(), resulting_nodes.copy()])
    

n_nodes, n_edges, n_edges_deg, coords, sizes, resulting_nodes = zip(*data)

coords = [np.array(c) for c in coords]
xlims = [[t_min-.5,t_max - 1.5] for t_min, t_max in zip(*zip(*t_min_max))]
sizes = [np.array(s) for s in sizes]


fig, (ax1, ax3) = plt.subplots(2, 1, figsize=(9, 9), dpi=300)
i_s = [np.argmax(c[:,0] < xlims[i][0]) for i, c in enumerate(coords)]
i_e = [np.argmax(c[:,0] > xlims[i][1]) for i, c in enumerate(coords)]

titles = [net_elab.team_name, "Cartesian net"]

nums = [8,4]

labspacs = [0.5,3]
bpads = [0.5,2]

for ax, coor, siz, nn, ne, title, s, e, num, labspac, bpad in zip((ax1, ax3),coords, sizes, n_nodes, n_edges, titles, i_s, i_e, nums, labspacs, bpads):
    ax.set_title(title)
    sc = ax.scatter(*coor[s:e].T, s=siz[s:e], c=nn[s:e], label = "Number of nodes", edgecolors="grey", linewidths=.2)
    ax.set_ylabel("Degree")
    ax2 = ax.twinx()
    ax2.set_ylabel("Number of edges")
    legend1 = ax.legend(*sc.legend_elements(prop="sizes",num=num), loc="upper right", labelspacing=labspac,  title="Number of nodes", borderpad=bpad)
    ax2.plot(thresholds, np.array(ne), linewidth=.75, color="black", label="Number of edges")
    legend2 = ax2.legend(loc="upper left")
    cbar = fig.colorbar(sc, ax=ax2, label="Total number of nodes", pad=.1)

ax1.set_xlabel("Duration threshold")
ax3.set_xlabel("$t_{dur}$")

f=figure_path("threshold_plot.png")

plt.tight_layout(h_pad=2, w_pad=0)
plt.savefig(f)
plt.plot()

# https://github.com/nschloe/tikzplotlib/issues/557

# def tikzplotlib_fix_ncols(obj):
#     """
#     workaround for matplotlib 3.6 renamed legend's _ncol to _ncols, which breaks tikzplotlib
#     """
#     if hasattr(obj, "_ncols"):
#         obj._ncol = obj._ncols
#     for child in obj.get_children():
#         tikzplotlib_fix_ncols(child)

# tikzplotlib_fix_ncols(fig)

# import tikzplotlib

# tikzplotlib.save("../text/threshold_plot.tex")



