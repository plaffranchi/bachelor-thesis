import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from match_data import get_data
from formations import get_formations
import matplotlib.colors as mcolors
from mplsoccer.pitch import VerticalPitch, Pitch

SCALE = .045
COLORS = ["#0000FF", "#FFA500", "#808080", "#800080", "#FF0000", "#FFF"]


class PitchPlot:
    def __init__(self, match_id, scale=SCALE, colors=COLORS, **kwargs):
        self.data = get_data(match_id)
        self.scale = scale
        self.colors = colors
        self.match_id = match_id
        self.colormap = mcolors.ListedColormap(colors)
        self.pitch_length, self.pitch_width = self.data["meta"]["PitchLongSide"] / \
            100, self.data["meta"]["PitchShortSide"]/100
        self.fig, self.ax = plt.subplots(
            figsize=(self.pitch_width*self.scale, self.pitch_length*self.scale))
        self.pitch = VerticalPitch(
            pitch_type='tracab', pitch_length=self.pitch_length, pitch_width=self.pitch_width, **kwargs)
        self.pitch.draw(ax=self.ax)

    def plot_points(self, pts, color="black", numbers=[], markersize=10, **kwargs):
        if len(numbers) > 0:
            for p in zip(*pts.T):
                for p, n in zip(pts, numbers):
                    self.pitch.plot(*p, ax=self.ax, linewidth=0,
                                    marker="o", markersize=markersize, color=color)
                    self.ax.text(*p[::-1], s=str(n), horizontalalignment="center",
                                 fontsize=7, verticalalignment="center", color="white", zorder=5)
                return self
        for p in zip(*pts.T):
            self.pitch.plot(*p, ax=self.ax, linewidth=0,
                            marker="o", markersize=markersize, color=color)

        return self

    def plot_points_with_role(self, pts, roles, numbers=None, **kwargs):
        unique_roles = np.unique(roles)
        for r in unique_roles:
            numbers_with_role = []
            indexes = (roles == r)
            if numbers is not None:
                numbers_with_role = numbers[np.flatnonzero(indexes)]
            self.plot_points(pts[:len(
                roles)][indexes], color=self.colors[r], numbers=numbers_with_role, **kwargs)
        return self

    def triplot(self, pts, triangles, color="grey"):

        self.ax.triplot(*pts.T[::-1], triangles, color=color)
        return self

    def tripcolor(self, pts, triangles, facecolors, cmap=None, alpha=.3, edgecolors="none", linewidth=0, antialiased=True, *args, **kwargs):
        if cmap is None:
            cmap = self.colormap
        self.ax.tripcolor(*pts.T, triangles, facecolors=facecolors, cmap=cmap, alpha=alpha,
                          edgecolors=edgecolors, linewidth=linewidth, antialiased=antialiased, *args, **kwargs)
        return self

    def plot_edge(self, pts, color, stability, second_stability, perc_stability, unstable, write_stability=False, **kwargs):
        style = "dotted" if unstable else None
        width = .5 + perc_stability
        self.pitch.plot(*pts.T, color=color, linestyle=style,
                        ax=self.ax, linewidth=5*width, solid_capstyle="round")
        midpoint = np.mean(pts, axis=0)
        if stability < np.inf:
            angle = np.arctan2(*np.diff(pts.T[::-1]))[0]
            pos = 1
            if angle < -np.pi/2:
                angle += np.pi
                pos = -1
            elif angle > np.pi/2:
                angle -= np.pi
                pos = -1
            angle = np.rad2deg(angle)
            angle -= 90

            if write_stability:
                self.ax.text(*midpoint.T[::-1], s=f"{stability:.00f}", fontsize=7, horizontalalignment="center",
                             verticalalignment="bottom", rotation=angle, rotation_mode="anchor", color="grey")
                if second_stability != stability:
                    angle += 180
                    self.ax.text(*midpoint.T[::-1], s=f"{second_stability:.00f}", fontsize=7,
                                 horizontalalignment="center", verticalalignment="bottom", rotation=angle, rotation_mode="anchor", color="grey")

        return self

    def plot_edges(self, pts, edges, stability, unstable, front=None, back=None, left=None, right=None, **kwargs):

        edges = edges | edges.T
        unstable = unstable | unstable.T

        if front is not None:
            front = front.T | front
        if back is not None:
            back = back.T | back

        if left is not None:
            left = left.T | left
        if right is not None:
            right = right.T | right

        max_stability = np.max(stability)
        min_stability = np.min(stability)

        perc_stability = (stability - min_stability) / \
            (max_stability - min_stability)
        for f, t in zip(*np.nonzero(np.triu(edges))):
            color = "black"
            if front is not None and front[t, f]:
                color = self.colors[4]

            if back is not None and back[f, t]:
                color = self.colors[0]

            if left is not None and left[f, t]:
                color = self.colors[1]

            if right is not None and right[f, t]:
                color = self.colors[3]

            self.plot_edge(
                pts[[f, t]],
                color=color,
                stability=stability[f, t],
                second_stability=stability[t, f],
                perc_stability=perc_stability[f, t],
                unstable=unstable[f, t],
                **kwargs
            )

        return self

    def draw_points(self, n_pts=10, pts=None, **kwargs):
        if pts is None:
            pts = []

        def onclick(event):
            self.plot_points(np.array([[event.ydata, event.xdata]]))
            self.fig.canvas.draw()
            pts.append([event.ydata, event.xdata])
            if len(pts) == n_pts:
                plt.close()
                return np.array(pts), np.arange(n_pts)

        cid = self.fig.canvas.mpl_connect("button_press_event", onclick)
        plt.show()
        return np.array(pts), np.arange(n_pts)

    def get_match_points(self, team, frame=None, alive_frame=None, **kwargs):
        alive = self.data["alive"]
        team_np = self.data[team]
        coords = team_np["coords_team_pov"][alive]
        on_field = team_np["on_field"][alive]
        frames = self.data["frames"][alive] - self.data["frames"][0]

        if frame is None:
            frame = np.argmax(frames == alive_frame)

        on_field = np.copy(on_field)
        on_field[:, np.isin(team_np["nos"], team_np["goalkeepers"])] = False
        pts = coords[frame, on_field[frame]]

        return np.copy(pts), np.copy(team_np["nos"][on_field[frame]])

    def show(self):
        plt.show()

    def export(self, f):
        mpl.rcParams.update({
            "pgf.texsystem": "pdflatex",
            'font.family': 'serif',
            'font.size': 11,
            'text.usetex': True,
            'pgf.rcfonts': False,
        })
        plt.tight_layout(pad=0)
        plt.savefig(f)
        mpl.rcParams.update(mpl.rcParamsDefault)


class BarycenterPitchPlot(PitchPlot):
    def __init__(self, team, *args,  **kwargs):
        from formation_barycenter import compute_formation
        self.compute_formation = compute_formation
        self.team = team
        super().__init__(*args, **kwargs)

    def plot_all(self, pts, plot_centers=False, **kwargs):
        _, roles, edges, triag_edges, stability, front, back, centers = self.compute_formation(
            pts)
        self\
            .plot_edges(pts, triag_edges, stability,  (~edges.T & ~edges) & triag_edges, front, back, **kwargs)\
            .plot_points_with_role(pts, roles, **kwargs)
        if plot_centers:
            self.plot_points(centers[edges], color="green")

    def plot_drawn_pts(self, plot_nos=False, **kwargs):
        pts, nos = self.draw_points(**kwargs)
        self.__init__(self.team, self.match_id)
        if plot_nos:
            self.plot_all(pts, numbers=nos, **kwargs)
        else:
            self.plot_all(pts, **kwargs)

    def plot_frame(self, **kwargs):
        pts, nos = self.get_match_points(self.team, **kwargs)
        self.plot_all(pts, numbers=nos, **kwargs)

