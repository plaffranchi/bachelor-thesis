
from add_parent_dir import *
import matplotlib as mpl
from os.path import isfile
from formation_barycenter import compute_formation
import numpy as np
from match_data import get_data
from drawings.pitch_plot import BarycenterPitchPlot
from settings import figure_path





if __name__=="__main__":

    f = figure_path("formation_too_few_removed.pdf")
    frames = {
        figure_path("formation_too_much_removed.pdf"): 105_825,
        figure_path("formation_too_few_removed.pdf"): 20_203,
    }
    frame = frames[f]
    # frame = 117_392
    # frame = 0
    # frame = 200
    # frame= 11997
    # frame = 2415
    # frame = 303

    drawn_history_path = local_fname("drawn.npy")

    if isfile(drawn_history_path):
        drawn = np.load(drawn_history_path)

    else:
        drawn = np.array([])


    n_players = 10




    # pts, nos = get_match_points(56, 0, frame)
    # pts, nos = draw_points()
    pts = np.array([
        [-3000, 530],
        [-3000, 1600],
        [-3000, -1600],
        [-3000, -530],
        [-1000, 2500],
        [-1000, 833],
        [-1000, -833],
        [-1000, -2500],
        [1000, 800],
        [1000, -800],

    ])


    f=figure_path("example_formation_def.pdf")
    # f=figure_path("example_formation.pdf")
    # f=figure_path("example_form_roles.pdf")

    # pts = drawn[-20:].reshape(10,2)
    # nos = np.arange(1,11)

    # roles = np.array([
    #     0,0,0,0,-1,-1,-1,-1,4,4
    # ])
    # roles = np.array([
    #     0,0,0,0,1,2,2,3,4,4
    # ])

    pplot = BarycenterPitchPlot(team=1, match_id=56)
    # pplot.plot_drawn_pts(write_stability=True)
    # pplot.export("random.pdf")
    # pplot.show()

    # pplot.plot_frame(alive_frame=10_000)
    # pplot.plot_points(pts[:4], pplot.colors[0])
    # pplot.plot_points(pts[4:8], "black")
    # pplot.plot_points(pts[8:], pplot.colors[4])
    pplot.export(figure_path("empty.png"))
    pplot.export(f)
    pplot.show()

    


    # code, roles, edges, triag_edges, stability, front, back, centers = compute_formation(pts)
    # pplot = BarycenterPitchPlot(1, 56, pad_left=0, pad_top=0, pad_bottom=0, pad_right = 0)
    # pplot.plot_edges(pts, triag_edges, stability,  (~edges.T & ~edges) & triag_edges, front, back).plot_points_with_role(pts, roles, numbers=nos)
    # pplot.plot_edges(pts, triag_edges, stability,  (~edges.T & ~edges) & triag_edges, front, back).plot_points_with_role(pts, roles)

    # pplot.plot_points_with_role(pts, roles=roles, markersize=11)
    # pplot.plot_points(centers[edges],color="green")

    # pplot.export(f)
    # pplot.export("../text/figures/example_form_triang.pdf")
    # pplot.export("random.pdf")
    # pplot.show()

    last_pts = drawn[-20:].reshape(10,2)
    if np.any(last_pts!=pts):
        drawn = np.append(drawn, pts)

        np.save(drawn_history_path, drawn)

