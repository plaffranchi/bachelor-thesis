class ProgressBar():
# Thanks to https://stackoverflow.com/a/37630397
    
    def __init__(self, text, total, n_threads, bar_length=40):
        self.total = total
        self.bar_length = bar_length
        self.amount = 0
        self.increment=total/100//n_threads
        self.text = text

        # self._lock = LockProc()

        self.print(increment=0)

    def is_to_update(self, i, verbose):
                i+=1
                return verbose and not i%self.increment
    def print(self, increment=None):
                if increment is None:
                    increment = self.increment
                self.amount+=increment
                if self.amount > self.total:
                    self.amount = self.total

                fraction = self.amount / self.total

                arrow = int(fraction * self.bar_length - 1) * '-' + '>'
                padding = int(self.bar_length - len(arrow)) * ' '

                ending = '\n' if self.amount == self.total else '\r'

                print(f'{self.text}: [{arrow}{padding}] {int(fraction*100)}%', end=ending)

    def terminate(self):
        self.print(self.total - self.amount)