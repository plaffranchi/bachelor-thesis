from os.path import join
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from time import sleep, time
from copy import deepcopy
from formations import get_formations
import settings



def printdiff(msg, a, b):
    print(f"{msg}:", a,"-",a - b,"=",b)







class Edges(np.ndarray):
    @property
    def distr(self):
        return np.unique(self, return_counts=True)
    
    def subset(self, mask):
        return self[mask].view(Edges)


class BaseNet:
    """
    BaseNet class for transition network between (soccer) formations.

    Attributes:
        - goal_names (list of str): Class-level attribute to define goal-related names, which includes "scored" and "conceded".

    Methods:
        - `__init__(self, tstamps, codes_ids, get_period_and_minute, fps, phases, goals_tstamps, ball_position, players_jersie_number, roles, keep_phases_edges=False, *args, **kwargs)`:
            Initialize a BaseNet instance with the provided input data and perform initial setup for network processing.

        - `init_dead(self)`:
            Initialize the dead edges array.

        - `flat_nodes(self)`:
            Return flattened nodes, i.e., all node IDs in the network.

        - `nodes(self)`:
            Return unique nodes IDs in the network.

        - `degree(self)`:
            Return the nodes' IDs and their degree, defined as the maximum between out-degree and in-degree.

        - `n_nodes(self)`:
            Return the total number of nodes in the network.

        - `codes(self)`:
            Return the formation codes associated with the nodes.

        - `edges_codes(self)`:
            Return the formation codes associated with each edge.

        - `str_nodes(self)`:
            Return a list of strings representing the formation code of the nodes.

        - `time(self, weight)`:
            Compute total and mean time values for nodes based on given edge weights.

        - `time_nodes(self)`:
            Return the total and mean time for all nodes.

        - `alive_time_nodes(self)`:
            Return the total and mean time for "alive" edges.

        - `dead_time_nodes(self)`:
            Return the total and mean time for "dead" edges.

        - `n_edges(self)`:
            Return the total number of edges in the network.

        - `is_loop(self)`:
            Determine if an edge is a loop (connects a node to itself).

        - `loop_sizes(self)`:
            Return edge sizes for loop edges.

        - `non_loop_sizes(self)`:
            Return edge sizes for non-loop edges.

        - `dead_loop(self)`:
            Return dead loop edges.

        - `alive_loop_sizes(self)`:
            Return edge sizes for "alive" loop edges.

        - `dead_loop_sizes(self)`:
            Return edge sizes for "dead" loop edges.

        - `tot_stationary_time_alive(self)`:
            Calculate the total stationary time for "alive" edges with loops.

        - `tot_transition_time_alive(self)`:
            Calculate the total transition time for "alive" edges without loops.

        - `tot_stationary_time(self)`:
            Calculate the total stationary time for all edges with loops.

        - `tot_transition_time(self)`:
            Calculate the total transition time for all edges without loops.

        - `data_ball_position(self)`:
            Process and return ball position data.

        - `edges_roles(self)`:
            Determine the roles of players for each edge.

        - `edges_goals(self)`:
            Extract goal-related information for each edge, including goals scored, goals conceded, and total goals.

        - `possession_edges(self)`:
            Compute a mask for possession edges.

        - `to_seconds(self, frames)`:
            Convert frames to seconds based on frames per second (fps).

        - `to_minutes(self, frames)`:
            Convert frames to minutes based on frames per second (fps).

        - `ids_to_codes(ids)`:
            Static method to convert node IDs to formation codes.

        - `possession_str(code)`:
            Static method to retrieve the possession information from a formation code.

        - `get_formation_str(code)`:
            Static method to create a formatted string representation of a formation code.

        - `stringify(code)`:
            Static method to create a string representation of a formation code.

        - `copy(self)`:
            Create a deep copy of the BaseNet instance.

        - `update_ids_weights(self)`:
            Update edge IDs and weights based on time differences.

        - `get_new_net(self, inplace=False, *args, **kwargs)`:
            Create a new BaseNet instance, optionally in-place, with the specified arguments and keyword arguments.

        - `collapse_edges(self, mask, *args, **kwargs)`:
            Collapse edges based on a specified mask.

        - `remove_phases_edges(self, *args, **kwargs)`:
            Remove edges between phases (periods) based on phase edges removal conditions.

        - `print_new_number_edges(self, old_n_edges, new_n_edges, verbose=True, *args, **kwargs)`:
            Print the difference in the number of edges before and after an operation.

        - `add_zero_loops(self, *args, **kwargs)`:
            Add zero duration loop edges.

        - `collapse_loops(self, *args, **kwargs)`:
            Collapse loop edges based on specified arguments.

        - `compute_remove_loops(self, threshold)`:
            Compute a mask for removing loop edges based on a specified threshold.

        - `remove_inplace_kwarg(self, kwargs)`:
            Remove the 'inplace' keyword from a dictionary of keyword arguments.

        - `remove_loops(self, threshold, *args, **kwargs)`:
            Remove loop edges based on a specified threshold.

        - `compute_small_degree(self, threshold=1)`:
            Compute a mask to remove nodes with a degree less than or equal to a specified threshold.

        - `remove_small_degree(self, threshold=1, iterate=True, *args, **kwargs)`:
            Remove nodes with a degree less than or equal to a specified threshold.

        - `remove_possession_edges(self, *args, **kwargs)`:
            Remove possession edges based on specific conditions.

        - `get_to_conv(self, kwargs, to_conv=None)`:
            Identify keys in a dictionary of keyword arguments that require unit conversion.

        - `add_minute(self, data, *args, **kwargs)`:
            Convert time-related data in a dictionary from frames to minutes.

        - `add_second(self, data, *args, **kwargs)`:
            Convert time-related data in a dictionary from frames to seconds.

        - `offensive_ratio(self, code)`:
            Calculate the offensive ratio of a formation code.

        - `sides(self, code)`:
            Determine the sides value based on a formation code.

        - `node_data(self, code, prefix=None, suffix=None)`:
            Generate a dictionary containing information about a node, based on a formation code.

        - `get_node_kwargs(self, str_node, code, tot_time, mean_time, alive_tot_time, alive_mean_time, dead_tot_time, dead_mean_time, degree)`:
            Create a dictionary with keyword arguments for a node based on specified data.

        - `add_edges_roles(self, kwargs, edge_roles)`:
            Add edge roles to the keyword arguments.

        - `get_nx_graph(self, remove_possession_edges=True, **kwargs_)`:
            Create a NetworkX MultiDiGraph representation of the network.

        - `export(self, fname, **kwargs)`:
            Export the network data to a GraphML file.

    """
     
    goal_names = ["scored", "conceded"]

    def __init__(self, tstamps, codes_ids, get_period_and_minute, fps, phases, goals_tstamps, ball_position, players_jersie_number, roles, keep_phases_edges = False, *args, **kwargs):
        """
        Initialize a BaseNet instance with the provided input data.

        Args:
            - `tstamps` (array-like): Timestamps of events.
            - `codes_ids` (array-like): Ids of the formations codes.
            - `get_period_and_minute` (function): A function to retrieve period and minute of the match.
            - `fps` (int): Frames per second (a measure of the data's time resolution).
            - `phases` (array-like): Phases of the data.
            - `goals_tstamps` (array-like): Timestamps of goal events.
            - `ball_position` (array-like): Position data of the ball.
            - `players_jersie_number` (array-like): Jersey numbers of players.
            - `roles` (array-like): Roles of players (0  = defender, 1 = LM, 2 = CM, 3 = RM, 4 = forward).
            - `keep_phases_edges` (bool, optional): Flag to determine whether to keep phase edges. Default is False.

        Other Parameters:
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.

        This constructor initializes a BaseNet instance with the provided data. It sets up various attributes and computes edge-related information for network analysis.

        The `breaks` attribute is computed based on the provided phases, and the `remove_phases_nodes` attribute is initialized as a boolean matrix. The decision to keep phase edges is controlled by the `keep_phases_edges` flag.

        The `edges_tstamps` and `edges_ids` attributes have shape (n_edges, 2) and they store the edges timestamps and (formation) ids respectively. 
        For each of the edge the value at index `0` (e.g. `edges_tstamps[:,0]`) contains the value at the start of the transition,
        while index `1` (e.g. `edges_tstamps[:,1]`) at the end of the transition.
        Edge weights are computed based on time differences. The `update_ids_weights` method is called to further process these IDs and weights.

        - `edges_dead` is computed using the `init_dead` method, and edges are separated into `alive` and `dead` categories based on this condition.

        The `alive_edges_weights` and `dead_edges_weights` attributes are created accordingly. The `N` attribute is not set in the constructor.

        Additional methods can be used to perform network analysis on the initialized BaseNet instance.
        """

        # Initialize instance variables with provided inputs
        self._tstamps = tstamps
        self._codes_ids = codes_ids
        self.fps = fps
        self.get_period_and_minute = get_period_and_minute
        self.ball_position = ball_position
        self.phases = phases
        self.goals_tstamps = goals_tstamps
        # Determine whether to keep edges between phases
        self.keep_phases_edges = keep_phases_edges

        # Compute breaks in the phases
        self.breaks = np.stack([phases[:-1, 1], phases[1:, 0]], axis=1)

        # Initialize a boolean matrix for removing phase nodes
        self.remove_phases_nodes = np.zeros((len(self.breaks), 2), dtype=np.bool_)

        # Store roles and player jersey numbers
        self.roles = roles
        self.players_jersie_number = players_jersie_number

        # Calculate time intervals between consecutive timestamps
        self.edges_tstamps = np.stack([self._tstamps[:-1], self._tstamps[1:]], axis=1)

        # Get corresponding IDs for the calculated edges
        self.edges_ids = self._codes_ids[self.edges_tstamps]

        # Compute weights based on time differences
        self.edges_weights = np.diff(self.edges_tstamps, axis=1).ravel()

        # Call the 'update_ids_weights' method to further process IDs and weights
        self.update_ids_weights()

        # Initialize dead edges and compute 'edges_dead'
        edges_dead = self.init_dead()

        # Separate edges into alive and dead based on 'edges_dead' condition
        self.alive_edges_weights = np.where(edges_dead, 0, self.edges_weights)
        self.dead_edges_weights = np.where(edges_dead, self.edges_weights, 0)

    def init_dead(self):
        """
        Initialize and return a boolean mask indicating dead edges based on a predefined condition.

        Returns:
            - (array-like): Boolean mask indicating dead edges.
        """
        return self.edges_weights > 1
        
    
        
            
    @property
    def flat_nodes(self):
        """
        Get the flattened nodes IDs.

        Returns:
            - (array-like): Flattened nodes IDs.
        """
        return self.edges_ids.flatten()

    @property
    def nodes(self):
        """
        Get unique nodes in the network.

        Returns:
            - (array-like): Unique nodes in the network.
        """
        return np.unique(self.flat_nodes)
    
    @property
    def degree(self):
        """
        Get nodes' IDs and their degrees.

        Returns:
            - (array-like): Nodes IDs.
            - (array-like): Nodes' degrees (maximum between out-degree and in-degree).
        """
        nodes = self.nodes
        out_nodes, out_deg = np.unique(self.edges_ids[:,0], return_counts=True)
        in_nodes, in_deg = np.unique(self.edges_ids[:,1], return_counts=True)

        deg = np.zeros_like(nodes)
        deg[np.isin(nodes, out_nodes)] = out_deg
        in_index = np.isin(nodes, in_nodes)
        lt = deg[in_index] < in_deg
        in_index &= lt
        deg[in_index] = in_deg[lt]

        return nodes, deg

    @property
    def n_nodes(self):
        """
        Get the total number of nodes in the network.

        Returns:
            - (int): Total number of nodes.
        """
        return len(self.nodes)
    
    @property
    def codes(self):
        """
        Get the formation codes corresponding to the nodes.

        Returns:
            - (array-like): Formation codes for nodes.
        """
        return self.ids_to_codes(self.nodes)
    
    @property
    def edges_codes(self):
        """
        Get the formation codes for each edge.

        Returns:
            - (array-like): Formation codes for edges.
        """
        return np.apply_along_axis(self.ids_to_codes, 1, self.edges_ids)
    
    @property
    def str_nodes(self):
        """
        Get string representations of the nodes.

        Returns:
            - (list): String representations of nodes.
        """
        return [self.stringify(c) for c in self.codes]
    
    def time(self, weight):
        """
        Compute total and mean times for nodes based on edge weights.

        Args:
            - `weight` (array-like): Edge weights.

        Returns:
            - (array-like): Total time for each node.
            - (array-like): Mean time for each node.
        """

        total = np.zeros_like(self.nodes)
        mean = np.zeros_like(self.nodes)
        for i, n in enumerate(self.nodes):
            edges_weights = (weight[(self.edges_ids[:,0] == n) & self.is_loop])
            if len(edges_weights)==0:
                edges_weights = [0]
            total[i] = np.sum(edges_weights)
            mean[i] = np.mean(edges_weights)

        return total, mean
    
    def time(self, weight):
        total = np.zeros_like(self.nodes)
        mean = np.zeros_like(self.nodes)
        for i, n in enumerate(self.nodes):
            edges_weights = (weight[(self.edges_ids[:,0] == n) & self.is_loop])
            total[i] = np.sum(edges_weights)
            mean[i] = np.mean(edges_weights)

        return total, mean
    
    @property
    def time_nodes(self):
        return self.time(self.edges_weights)
    
    @property
    def alive_time_nodes(self):
        return self.time(self.alive_edges_weights)

    @property
    def dead_time_nodes(self):
        return self.time(self.dead_edges_weights)

    @property
    def time_nodes(self):
        """
        Get total and mean times for nodes based on edge weights.

        Returns:
            - (array-like): Total time for each node.
            - (array-like): Mean time for each node.
        """
        return self.time(self.edges_weights)
    
    @property
    def alive_time_nodes(self):
        """
        Get total and mean times for nodes based on alive edge weights.

        Returns:
            - (array-like): Total time for each node.
            - (array-like): Mean time for each node.
        """

        return self.time(self.alive_edges_weights)

    @property
    def dead_time_nodes(self):
        """
        Get total and mean times for nodes based on dead edge weights.

        Returns:
            - (array-like): Total time for each node.
            - (array-like): Mean time for each node.
        """
        return self.time(self.dead_edges_weights)

    @property
    def n_edges(self):
        """
        Get the total number of edges in the network.

        Returns:
            - (int): Total number of edges.
        """
        return len(self.edges_tstamps)
    
    @property
    def is_loop(self):
        """
        Get a boolean mask indicating if edges are loops.

        Returns:
            - (array-like): Boolean mask for loop edges.
        """
        return self.edges_ids[:,0] == self.edges_ids[:,1]

    
    @property
    def loop_sizes(self):
        """
        Get the weights of loop edges.

        Returns:
            - (array-like): Weights of loop edges.
        """
        return self.edges_weights[self.is_loop].view(Edges)
    
    @property
    def non_loop_sizes(self):
        """
        Get the weights of non-loop edges.

        Returns:
            - (array-like): Weights of non-loop edges.
        """
        return self.edges_weights[~self.is_loop].view(Edges)
    
    @property
    def dead_loop(self):
        """
        Get the dead loop edges.

        Returns:
            - (array-like): Dead loop edges.
        """
        return self.edges_ids[(self.alive_edges_weights == 0) & self.is_loop].view(Edges)
    
    @property
    def dead_edges(self):
        return self.alive_edges_weights == 0

    @property
    def loop_alive_sizes(self):
        """
        Get the alive weights of loop edges.

        Returns:
            - (array-like): Weights of alive loop edges.
        """
        return self.alive_edges_weights[self.is_loop].view(Edges)
    
    @property
    def alive_loop_sizes(self):
        """
        Get the weights of alive loop edges.

        Returns:
            - (array-like): Weights of alive loop edges.
        """
        return self.edges_weights[self.is_loop & ~self.dead_edges].view(Edges)
    
       

    
    @property
    def loop_dead_sizes(self):
        """
        Get the weights of dead loop edges.

        Returns:
            - (array-like): Weights of dead loop edges.
        """
        return self.edges_weights[self.is_loop & self.dead_edges].view(Edges)
    
    @property
    def tot_stationary_time_alive(self):
        """
        Calculate the total alive stationary time, as a sum of all loops' alive weights.

        Returns:
            - (float): Total alive stationary time for loops.
        """
        return self.alive_edges_weights[self.is_loop].sum()
    
    @property
    def tot_transition_time_alive(self):
        """
        Calculate the total alive transition time, as a sum of all non-loops' alive weights.

        Returns:
            - (float): Total alive transition time for edges.
        """
        return self.alive_edges_weights[~self.is_loop].sum()
    
    @property
    def tot_stationary_time(self):
        """
        Calculate the total stationary time, as a sum of all loops' weights.

        Returns:
            - (float): Total stationary time for loops.
        """
        return self.edges_weights[self.is_loop].sum()
    
    @property
    def tot_transition_time(self):
        """
        Calculate the total transition time, as a sum of all non-loops' weights.

        Returns:
            - (float): Total transition time for edges.
        """
        return self.edges_weights[(~self.is_loop)].sum()
    
    @property
    def data_ball_position(self):
        """
        Process and return ball position data.

        Returns:
            - (array-like): Mean ball position during the transition.
            - (array-like): Displacement of the ball between the start and the end of the transition.
        """

        mean_ball_position = np.zeros_like(self.edges_ids, dtype=np.float64)
        delta_ball_position = np.zeros_like(mean_ball_position)
        for i, (s, e) in enumerate(self.edges_tstamps):
            mean_ball_position[i] = np.mean(self.ball_position[s:e].astype(np.float64), axis=0)
            delta_ball_position[i] = np.diff(self.ball_position[[s,e]].astype(np.float64), axis=0)
        return mean_ball_position, delta_ball_position
    
    @property
    def edges_roles(self):
        """
        Determine the roles of players for each edge.

        Returns:
            - (array-like): Roles of players for each edge.
        """

        return self.roles[self.edges_tstamps]


    
    @property
    def edges_goals(self):
        """
        Extract goal-related information for each edge.

        Returns:
            - (array-like): Goals scored for each edge.
            - (array-like): Goals conceded for each edge.
            - (array-like): Total goals (both scored and conceded) for each edge.
        """

        def get_goals_edges(goals):
            gs = np.zeros_like(self.edges_ids, np.int8)
            for i, g in enumerate(np.sort(goals)):
                n_goals = i+1
                row, col = np.nonzero(self.edges_tstamps > g)
                row, col = row[0], col[0]
                if row == 0:
                    gs[row:] = n_goals
                else:
                    gs[row, col] = n_goals
                    gs[row+1:] = n_goals
            return gs
        goals_scored = get_goals_edges(self.goals_tstamps[0])
        goals_conceded = get_goals_edges(self.goals_tstamps[1])
        goals_total = goals_scored + goals_conceded

        return goals_scored, goals_conceded, goals_total

    @property
    def possession_edges(self):
        diff = np.diff(self.edges_codes, axis=1)[:, 0, -1]
        mask = np.abs(diff)==1
        return mask

    
    def to_seconds(self, frames):
        """
        Convert frames to seconds based on frames per second (fps).

        Args:
            - `frames` (array-like): Frames to convert to seconds.

        Returns:
            - (array-like): Time values in seconds.
        """
        return frames/self.fps
    
    def to_minutes(self, frames):
        """
        Convert frames to minutes based on frames per second (fps).

        Args:
            - `frames` (array-like): Frames to convert to minutes.

        Returns:
            - (array-like): Time values in minutes.
        """
        return self.to_seconds(frames)/60
    
    @staticmethod
    def ids_to_codes(ids):
        """
        Convert node IDs to formation codes.

        Abstract and static method.

        Args:
            - `ids` (array-like): Node IDs to convert.

        Returns:
            - (array-like): Formation codes.
        """
        pass

    @staticmethod
    def possession_str(code):
        """
        Retrieve the possession string from a formation code.

        Args:
            - `code` (str): Formation code.

        Returns:
            - (str): Possession string.
        """
        return str(code[-1])
    
    @staticmethod
    def get_formation_str(code):
        """
        Create a formatted string representation of a formation code.

        Args:
            - `code` (str): Formation code.

        Returns:
            - (str): Formatted representation of the formation code.
        """
        if code.sum() == 0 or code[0] == -1:
            return "invalid"  
        cstr = code.astype(str)
        cstr[1] = f"({cstr[1]}"
        cstr[3] = f"{cstr[3]})"
        return "-".join(cstr[:5])

    @staticmethod
    def stringify(code):
        """
        Create a string representation of a formation code.

        Args:
            - `code` (str): Formation code.

        Returns:
            - (str): String representation of the formation code.
        """
        if code[0] == -1 or code.sum() == 0:
            return f"invalid {code[-1]}"
        return "/".join([BaseNet.get_formation_str(code),BaseNet.possession_str(code)])
    

    def copy(self):
        """
        Create a deep copy of the BaseNet instance.

        Returns:
            - (BaseNet): Deep copy of the current BaseNet instance.
        """
        return deepcopy(self)
    
    def update_ids_weights(self):
        """
        Update edge IDs and weights based on timestamps.
        """
        self.edges_ids = self._codes_ids[self.edges_tstamps]
        self.edges_weights = np.diff(self.edges_tstamps, axis=1).ravel()
        if hasattr(self, "alive_edges_weights") and hasattr(self, "dead_edges_weights"):
            sum_ad = (self.alive_edges_weights + self.dead_edges_weights)

            assert np.all(sum_ad == self.edges_weights)

    def get_new_net(self, inplace = False, *args, **kwargs):
        """
        Create a new BaseNet copy if not in-place, otherwise return self.

        Args:
            - `inplace` (bool, optional): Flag to determine whether to create a new instance copy. Default is False.
            - `*args`: Ignored.
            - `**kwargs`: Ignored.

        Returns:
            - (BaseNet): A new BaseNet instance with the specified settings.
        """
        return self if inplace else self.copy()

    def collapse_edges(self, mask, *args, **kwargs):
        """
        Collapse edges based on a specified mask.

        Args:
            - `mask` (array-like): Boolean mask for edge collapsing.
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.
        
        Returns:
            - (BaseNet): a BaseNet with the collapsed loop. If `inplace==True`, the same instance is returned, otherwise a deepcopy.
        """
        last_tstamp = self.edges_tstamps[-1,1]
        new_net = self.get_new_net(*args, **kwargs)

        if np.all(mask[1:]) or np.all(~mask):
            # If all of None of the edges are to collapse, return the same net
            return new_net

        to_remove = np.copy(mask)
        new_net = self.get_new_net(*args, **kwargs)


        if not self.keep_phases_edges:
            for i, (s, e) in enumerate(self.breaks):
                break_edge = (self.edges_tstamps[:, 0] <= s) & (self.edges_tstamps[:, 1] >= e)
                if not np.any(break_edge):
                    continue
                br_e_i = np.flatnonzero(break_edge)[0]
                self.remove_phases_nodes[i, 0] |= to_remove[br_e_i-1] & to_remove[br_e_i]
                self.remove_phases_nodes[i, 1] |= to_remove[br_e_i+1] & to_remove[br_e_i]
                to_remove[br_e_i] = False

            

        

        diff = np.diff(to_remove.astype(np.int8))
        starts = np.flatnonzero(diff == 1) + 1
        ends = np.flatnonzero(diff == -1)




        consec = self.edges_tstamps[1:,0] == self.edges_tstamps[:-1,1]
        # In order to be collapsed, two edges must have consecutive timestamps
        to_remove[1:] &= consec


        if not self.keep_phases_edges:
            for i, (s, e) in enumerate(self.breaks):
                # Find edge between the two periods: "break" edge
                break_edge = (self.edges_tstamps[:, 0] <= s) & (self.edges_tstamps[:, 1] >= e)
                if not np.any(break_edge):
                    continue
                # If there's any edge is found, get its index
                br_e_i = np.flatnonzero(break_edge)[0]
                # If this and the previous (next) edges are to remove, set the node to remove.
                self.remove_phases_nodes[i, 0] |= to_remove[br_e_i-1] & to_remove[br_e_i]
                self.remove_phases_nodes[i, 1] |= to_remove[br_e_i+1] & to_remove[br_e_i]
                # Keep the break edge
                to_remove[br_e_i] = False


        diff = np.diff(to_remove.astype(np.int8))
        starts = np.flatnonzero(diff == 1) + 1
        ends = np.flatnonzero(diff == -1)

        # We keep the first edge and remove all the following ones
        to_remove[starts] = False
        to_remove[0] = False



        old_n_edges = self.n_edges


        if len(starts)>0 or len(ends)>0:
            remove_start  = len(starts) == 0 or starts[0]-1 > ends[0] 
            # This is `True` when the first collapse interval starts with the first edge (and it doesn't get removed)
            remove_end = len(ends) == 0 or ends[-1] < starts[-1]
            # This is `True` when the last collapse interval ends with the last edge (and it doesn't get removed)

            if remove_end:
                ends = np.append(ends, len(self.alive_edges_weights)-1)
            
            ends_tstamps = new_net.edges_tstamps[ends,1]
            # new_starts will contain the indices pointing to the collapsed edges (but not the first in the whole network)
            new_starts = np.zeros(old_n_edges, dtype=np.bool_)
            new_starts[starts] = True
            new_net.edges_tstamps = np.delete(self.edges_tstamps, to_remove, axis=0)
            new_starts = np.delete(new_starts, to_remove, axis=0)
            # We update all v, where e = (u,v), and the weights and ids
            if remove_start:
                # In this case, the first collapsed edge is the first edge in both networks
                new_net.edges_tstamps[new_starts,1] = ends_tstamps[1:]
                new_net.edges_tstamps[0,1] = ends_tstamps[0]
            else:
                new_net.edges_tstamps[new_starts,1] = ends_tstamps
            
            new_net.edges_tstamps[-1,1] = last_tstamp

            def update_ad_weights(orig_weights):
                weights = np.copy(orig_weights)
                # Use the difference of the cumulative sum to define compute the new weights efficiently
                orig_weights_cs = np.cumsum(orig_weights)
                if remove_start:
                    try:
                        assert starts.shape[0]+1 == ends.shape[0]
                    except AssertionError:
                        raise Exception("Shape didn't match:", starts.shape[0], ends.shape[0])
                    
                    # If the first edge has been collapsed, the new weights is just the sum of the first collapsed edges
                    weights[0] = orig_weights_cs[ends[0]]
                    weights[starts] = orig_weights_cs[ends[1:]] - orig_weights_cs[starts-1]


                else:
                    try:
                        assert starts.shape == ends.shape
                    except AssertionError:
                        raise Exception("Shape didn't match:", starts.shape, ends.shape)
                    
                    weights[starts] = orig_weights_cs[ends] - orig_weights_cs[starts-1]
                weights = np.delete(weights, to_remove, axis=0)
                return weights
            
            # Update both alive and dead weights, using same procedure
            new_net.alive_edges_weights = update_ad_weights(self.alive_edges_weights)
            new_net.dead_edges_weights = update_ad_weights(self.dead_edges_weights)

            new_net.update_ids_weights()

            if len(new_net.edges_ids)>1:
                if remove_start:
                    # If the start was to remove, substitute the code of the last timestamp with the code of the next edge
                    new_net._codes_ids[new_net.edges_tstamps[0,0]] = new_net.edges_ids[0,1]
                
                if remove_end:
                    # If the end was to remove, substitute the code of the last timestamp with the code of the previous edge
                    new_net._codes_ids[new_net.edges_tstamps[-1,1]] = new_net.edges_ids[-1,0]


        
        new_net.update_ids_weights()       

            
        self.print_new_number_edges(old_n_edges, new_net.n_edges, *args, **kwargs)
        
        return new_net
    
    def remove_phases_edges(self, *args, **kwargs):
        """
        Remove edges between phases (periods) based on phase edges removal conditions.

        Args:
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.
        """

        new_net = self.get_new_net(*args, **kwargs)


        for s, e, r_s, r_e in zip(*self.breaks.T, *self.remove_phases_nodes.T):
            break_edge = (self.edges_tstamps[:, 0] <= s) & (self.edges_tstamps[:, 1] >= e)
            if not np.any(break_edge):
                continue
            i = np.flatnonzero(break_edge)[0]
            if r_s:
                new_net._codes_ids[new_net.edges_tstamps[i-1, 1]] = new_net._codes_ids[new_net.edges_tstamps[i-1, 0]]
            if r_e:
                new_net._codes_ids[new_net.edges_tstamps[i+1, 0]] = new_net._codes_ids[new_net.edges_tstamps[i+1, 1]]

            # new_net.update_ids_weights()

            kw = self.remove_inplace_kwarg(kwargs)

            new_net.edges_tstamps = np.delete(new_net.edges_tstamps, i, axis=0)
            new_net.edges_ids = np.delete(new_net.edges_ids, i, axis=0)
            new_net.alive_edges_weights = np.delete(new_net.alive_edges_weights, i, axis=0)
            new_net.dead_edges_weights = np.delete(new_net.dead_edges_weights, i, axis=0)
            new_net.collapse_loops(inplace=True, **kw)

        
        return new_net
        

    
    def print_new_number_edges(self, old_n_edges, new_n_edges, verbose = True, *args, **kwargs):
        """
        Print the difference in the number of edges before and after an operation.

        Args:
            - `old_n_edges` (int): Number of edges before the operation.
            - `new_n_edges` (int): Number of edges after the operation.
            - `verbose` (bool, optional): Flag to control verbosity. Default is True.
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.
        """
        if verbose:
            printdiff("New number of edges", old_n_edges, new_n_edges)

    
    def add_zero_loops(self, *args, **kwargs):
        """
        Add zero-duration loop edges.

        Args:
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.
        """

        insert_indices = ~self.is_loop

        diff = np.diff(insert_indices.astype(np.int8))
        consec_edges_end = np.flatnonzero(diff == 1) + 1
        insert_indices[consec_edges_end] = False
        # `insert_indices` is True for each edge that goes to a node 
        # and the edge coming after is not a loop.
        # At those positions we will insert a new loop of 0 duration.
        insert_indices = np.flatnonzero(insert_indices)


        values = self.edges_tstamps[insert_indices,0]
        values = np.stack((values, values), axis=1)

        new_net = self.get_new_net(*args, **kwargs)

        new_net.edges_tstamps = np.insert(self.edges_tstamps,insert_indices, values, axis=0)
        new_net.alive_edges_weights = np.insert(self.alive_edges_weights,insert_indices, 0, axis=0)
        new_net.dead_edges_weights = np.insert(self.dead_edges_weights,insert_indices, 0, axis=0)
        new_net.update_ids_weights()

        return new_net

    def collapse_loops(self, *args, **kwargs):
        """
        Collapse loop edges based on specified arguments.

        Args:
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.
        """
        return self.collapse_edges(self.is_loop, *args, **kwargs)
    

    def compute_remove_loops(self, threshold: int):
        """
        Compute a mask for removing loop edges that have alive duration strictly smaller than the  threshold.

        Args:
            - `threshold` (int): Threshold for loop  removal.

        Returns:
            - (array-like): Boolean mask for loop removal.
        """
        mask = np.zeros_like(self.edges_weights, dtype=bool)
        mask[self.is_loop] = self.loop_alive_sizes < threshold
        return mask

    def remove_inplace_kwarg(self, kwargs: dict):
        """
        Remove the 'inplace' key from the input `kwargs` dictionary, if it exists.

        Args:
            - `kwargs` (dict): A dictionary containing key-value pairs, including a possible 'inplace' key.

        Returns:
            - dict: A new dictionary `kw` without the 'inplace' key, derived from `kwargs`.
        """
        kw = kwargs.copy()
        if "inplace" in kw:
            del kw["inplace"]
        return kw

    def remove_loops(self, threshold: int, *args, **kwargs):
        """
            Remove loops from the network data based on a given threshold and using the `compute_remove_loops` class method.

            This method removes loops from the network data by collapsing them based on the specified threshold.

            Args:
            - `threshold` (int): The threshold for loop removal (duration in number of keyframes).
            - `*args`: Additional positional arguments.
            - `**kwargs`: Additional keyword arguments.

            Returns:
            - `new_net` (Network object): The modified network data with loops removed.
        """
        new_net = self.add_zero_loops(*args, **kwargs)
        kw = self.remove_inplace_kwarg(kwargs)
        new_net.collapse_loops(inplace=True, **kw)
        mask = new_net.compute_remove_loops(threshold)
        edges_to_replace = np.zeros_like(new_net.edges_weights,dtype=bool)
        for i in np.flatnonzero(mask):
            # Set both the edges before and after the loop as to collapse
            edges_to_replace[i-1:i+2] = True
        new_net.collapse_edges(edges_to_replace, inplace=True, **kw)
        new_net.collapse_loops(inplace=True, **kw)
        return new_net

    
    def compute_small_degree(self, threshold=1):
        """
        Compute a mask to identify nodes with a degree less than or equal to the specified `threshold`.

        Args:
            - `threshold` (int, optional): The degree threshold. Nodes with degrees less than or equal to this value will be identified.

        Returns:
            - np.ndarray: A boolean mask identifying nodes with degrees less than or equal to the `threshold`.
        """
        nodes, degrees = self.degree
        nodes_to_delete = nodes[degrees <= threshold]
        mask = np.any(np.isin(self.edges_ids, nodes_to_delete), axis=1)
        return mask
    
    def remove_small_degree(self, threshold=1, iterate=True, *args, **kwargs):
        """
        Remove nodes with degrees less than or equal to the specified `threshold`.
        
        This method removes nodes with degrees less than or equal to the `threshold` and optionally iteratively continues removing nodes until all remaining nodes have degrees greater than the `threshold`.

        Args:
            - `threshold` (int, optional): The degree threshold. Nodes with degrees less than or equal to this value will be removed.
            - `iterate` (bool, optional): If `True`, the method will iteratively remove nodes until all remaining nodes have degrees greater than the `threshold`.
            - `*args` (tuple): Additional positional arguments for method calls.
            - `**kwargs` (dict): Additional keyword arguments for method calls.

        Returns:
            - BaseNet: A new instance of your class with nodes removed based on the degree threshold.
        """
        new_net = self.collapse_loops(*args, **kwargs)
        mask = new_net.compute_small_degree(threshold = threshold)
        kw = self.remove_inplace_kwarg(kwargs)

        new_net.collapse_edges(mask, inplace=True, *args, **kw)

        new_net.collapse_loops(inplace=True, **kw)
        new_mask = new_net.compute_small_degree(threshold)
        if iterate:
            while np.any(new_mask) and (new_mask.size != mask.size or np.any(new_mask != mask)):
                mask = new_mask
                new_net.collapse_edges(new_mask, inplace=True, **kw)
                new_net.collapse_loops(inplace=True, **kw)
                new_mask = new_net.compute_small_degree(threshold)
        return new_net
    
    def remove_possession_edges(self, *args, **kwargs):
        """
        Remove possession edges from the network.

        This method removes possession edges from the network based on a specified condition.

        Args:
            - `*args` (tuple): Additional positional arguments for method calls.
            - `**kwargs` (dict): Additional keyword arguments for method calls.

        Returns:
            - BaseNet: A new instance of your class with possession edges removed.
        """
        mask = self.possession_edges
        old_n_edges = self.n_edges
        new_net = self.get_new_net(*args, **kwargs)
        new_net.edges_tstamps = np.delete(self.edges_tstamps, mask, axis=0)
        new_net.alive_edges_weights = np.delete(self.alive_edges_weights, mask, axis=0)
        new_net.dead_edges_weights = np.delete(self.dead_edges_weights, mask, axis=0)
        new_net.update_ids_weights()

        self.print_new_number_edges(old_n_edges, new_net.n_edges, *args, **kwargs)
                
        return new_net
    
    def get_to_conv(self, kwargs, to_conv = None):
        """
        Get a list of keys in `kwargs` that need time unit conversion.

        Args:
            - `kwargs` (dict): A dictionary containing key-value pairs.
            - `to_conv` (list, optional): A pre-defined list of keys to convert. Defaults to None.

        Returns:
        - list: A list of keys that require time unit conversion.
        """
        if to_conv is not None:
            return to_conv
        to_conv = []
        for key in kwargs.keys():
            if "[fr]" in key:
                to_conv.append(key)
        return to_conv
        
    
    def add_minute(self, data, *args, **kwargs):
        """
        Convert time-related values in `data` from fr (number of frames) to minutes.

        Args:
            - `data` (dict): A dictionary containing key-value pairs with fr time units.
            - *args: Additional positional arguments.
            - **kwargs: Additional keyword arguments.
        """

        to_conv = self.get_to_conv(data, *args, **kwargs)                    
        for attr in to_conv:
            data[attr.replace("[fr]","[min]")] = self.to_minutes(data[attr])

    def add_second(self, data, *args, **kwargs):
        """
        Convert time-related values in `data` from kfr (kiloframes) to seconds.

        Args:
            - `data` (dict): A dictionary containing key-value pairs with kfr time units.
            - *args: Additional positional arguments.
            - **kwargs: Additional keyword arguments.
        """

        to_conv = self.get_to_conv(data, *args, **kwargs)                    
        for attr in to_conv:
            data[attr.replace("[fr]","[s]")] = self.to_seconds(data[attr])

    def offensive_ratio(self, code):
        """
        Calculate the offensive ratio from a given formation code.

        Args:
            - `code` (array-like): An array-like object representing a formation code.

        Returns:
            - float: The offensive ratio calculated from the `code`.
        """
        return (code[:5] @ np.array([0,1,1,1,2])) / (2*np.sum(code[:5]))

    def sides(self, code):
        """
        Returns a number that describes which side midfielder are in the formation code.
        Args:
            - `code` (array-like): Formation code.

        Returns:
            - 3: if both side midfielders are present.
            - 1: if only left midfielder is present.
            - 2: if only right midfielder is present.
            - 0: if no side midfielders are present.
        """
        return 3 if (code[1] and code[3]) else 1 if code[1] else 2 if code[3] else 0

    def node_data(self, code, prefix=None, suffix=None):
        """
        Extract information about a node based on a given `code`.

        This method parses a formation code to extract various properties and returns them as a dictionary.

        Args:
            - `code` (numpy.ndarray): The node code to extract information from.
            - `prefix` (str, optional): A prefix to be added to extracted property names.
            - `suffix` (str, optional): A suffix to be added to extracted property names.

        Returns:
            -   dict: A dictionary containing extracted properties with optional `prefix` and `suffix`.
        """

        data = {
            "in possession": bool(code[5]),
            "back": code[0],
            "left": (code[1]),
            "middle": code[2],
            "right": (code[3]),
            "forward": code[4],
            "sides": self.sides(code),
            "offensive ratio": self.offensive_ratio(code),
        }

        if suffix is not None or prefix is not None:
            data_ = {}
            for k in data.keys():
                components = []
                if prefix is not None:
                    components.append(prefix)
                components.append(k)
                if suffix is not None:
                    components.append(suffix)
                data_[" ".join(components)] = data[k]
            data = data_

        return data


    
    def get_node_kwargs(self, str_node, code, tot_time, mean_time, alive_tot_time, alive_mean_time, dead_tot_time, dead_mean_time, degree):

        """
        Generate a dictionary of keyword arguments for a node.

        Args:
            - `str_node` (str): A string representation of the node.
            - `code` (numpy.ndarray): The node's code containing various properties.
            - `tot_time` (float): Total time associated with the node.
            - `mean_time` (float): Mean duration time.
            - `alive_tot_time` (float): Total alive time.
            - `alive_mean_time` (float): Mean alive duration time.
            - `dead_tot_time` (float): Total dead time.
            - `dead_mean_time` (float): Mean dead duration time.
            - `degree` (int): The degree of the node.

        Returns:
            - dict: A dictionary of keyword arguments representing the node.
        """
        kwargs = self.node_data(code)

        kwargs |= {
            "code": str_node,
            "total time [fr]": tot_time,
            "degree": degree,
            "mean duration [fr]": mean_time,
            "alive total time [fr]": alive_tot_time,
            "alive mean duration [fr]": alive_mean_time,
            "dead total time [fr]": dead_tot_time,
            "dead mean duration [fr]": dead_mean_time,
        }

        return kwargs

    def add_edges_roles(self, kwargs, edge_roles):
        """
        Add edge role information to a dictionary of keyword arguments.

        This method updates a dictionary of keyword arguments to include edge role information for each player.

        Args:
            - `kwargs` (dict): The dictionary of keyword arguments to update.
            - `edge_roles` (numpy.ndarray): A matrix containing edge roles for players.

        """
        for i, n in enumerate(self.players_jersie_number):
            kwargs[f"start {n} role"], kwargs[f"end {n} role"] = edge_roles[:, i]
            kwargs[f"change {n} role"] = " to ".join([str(r) for r in edge_roles[:, i]])

            
    def get_nx_graph(self, remove_possession_edges = True, **kwargs_):
        """
        Create a NetworkX MultiDiGraph representation of the network.

        This method generates a NetworkX MultiDiGraph representation of the network data.

        Args:
            - `remove_possession_edges` (bool): Flag to remove possession edges.
            - `**kwargs_` (dict): Additional keyword arguments.

        Returns:
            - `nx_net` (networkx.MultiDiGraph): The generated NetworkX MultiDiGraph.
        """
    
        nx_net = nx.MultiDiGraph()

        nodes = []

         # Prepare the network data by collapsing loops and optionally removing phase edges.
        self.collapse_loops(inplace=True, verbose=False)

        if not self.keep_phases_edges:
            self.remove_phases_edges(inplace=True, verbose=False)

        # Remove possession edges if required.
        if remove_possession_edges:
            self.remove_possession_edges(inplace=True, verbose=False)

        ids, degrees = self.degree

        # Process nodes and generate node data.
        for id, args in zip(ids, zip(self.str_nodes, self.codes, *self.time_nodes, *self.alive_time_nodes, *self.dead_time_nodes, degrees)):
            kwargs = self.get_node_kwargs(*args)
            self.add_minute(kwargs)
            self.add_second(kwargs)
            nodes.append((int(id), kwargs))

        nx_net.add_nodes_from(nodes)

        # Extract periods and minutes from edge timestamps.
        periods_start, minutes_start = self.get_period_and_minute(self.edges_tstamps[:,0])
        periods_end, minutes_end = self.get_period_and_minute(self.edges_tstamps[:,1])

        # Extract edge data.
        ew = self.edges_weights
        aew = self.alive_edges_weights 
        dew = self.dead_edges_weights
        gs, gc, gtot = self.edges_goals
        mean_ball_position, delta_ball_position = self.data_ball_position
        mpb_x, mpb_y = mean_ball_position.T
        dpb_x, dpb_y = delta_ball_position.T

        possession_edges = self.possession_edges
        
        edges_roles = self.edges_roles

        # Process edges and generate edge data.
        for i, (e, e_c) in enumerate(zip(self.edges_ids, self.edges_codes)):
            start_time, end_time = self.edges_tstamps[i]
            kwargs = {
                "delta time [fr]": ew[i],
                "delta alive time [fr]": aew[i],
                "delta dead time [fr]": dew[i],
                "start time [fr]": start_time,
                "end time [fr]": end_time,
                f"start goals {self.goal_names[0]}": gs[i, 0],
                f"end goals {self.goal_names[0]}": gs[i, 1],
                f"start goals {self.goal_names[1]}": gc[i, 0],
                f"end goals {self.goal_names[1]}": gc[i, 1],
                "mean ball position x": mpb_x[i],
                "mean ball position y": mpb_y[i],
                "delta ball position x": dpb_x[i],
                "delta ball position y": dpb_y[i],
                "start goals total": gtot[i, 0],
                "end goals total": gtot[i, 1],
                "start period": periods_start[i],
                "start minute": minutes_start[i],
                "end period": periods_end[i],
                "end minute": minutes_end[i],
                "loop ": bool(self.is_loop[i]),
                "possession start": self.possession_str(e_c[0]),
                "possession end":  self.possession_str(e_c[1]),
            }
            
            # Add 'possession change' data if possession edges are not removed.
            if not remove_possession_edges:
                kwargs["possession change"] = bool(possession_edges[i])

            self.add_edges_roles(kwargs, edges_roles[i])

            # Convert time-related data to minutes and seconds.
            conv_time = ["delta time [fr]", "delta alive time [fr]", "delta dead time [fr]"]

            self.add_minute(kwargs, conv_time)
            self.add_second(kwargs, conv_time)

            # Add the edge to the NetworkX MultiDiGraph.
            nx_net.add_edge(
                *e, **kwargs                
            )
        return nx_net

    def export(self, fname, **kwargs):  
        """
        Export the network data to a GraphML file.

        This method exports the network data to a GraphML file with the specified filename.

        Args:
            - `fname` (str): The filename for the GraphML file.
            - `kwargs` (dict): Additional keyword arguments.

        Returns:
            - None
        """      
        nx.write_graphml_lxml(self.get_nx_graph(**kwargs), fname)




class TeamNet(BaseNet):

    bits_dtype = 32
    uint_dtype = f"uint{bits_dtype}"
    int_dtype = f"int{bits_dtype}"

    bits = [5,4,4,4,4,1]
    tot_bits = sum(bits)

    assert tot_bits <= bits_dtype

    def __init__(self, match_id = None, team = None, *args, **kwargs):

        formations, data = get_formations(match_id=match_id, team=team, *args, **kwargs)
        codes = formations["codes"]
        alive = data["alive"]
        frames = data["frames"] - data["frames"][0]
        
        self.team_id = data["get_team_id"](team)
        self.team_name = data["team_names"][self.team_id]

        goals_scored_tstamps = data["goals"][self.team_id]
        goals_conceded_tstamps = data["goals"][1-self.team_id]

        ball_position = data[team]["ball_position"]

        roles = np.zeros_like(data[team]["on_field"], dtype=np.int8) -1
        roles[alive] = formations["roles"]
        players_jersie_number = data[team]["nos"]


            


        get_period_and_minute = lambda frame: data["period_and_minute"](frame)

        self.l = len(codes)
        tstamps = frames[alive]

        codes_ids = np.zeros(frames[-1], dtype=TeamNet.uint_dtype)
        codes_ids_alive = self.codes_to_ids(codes)
        codes_ids[tstamps] = codes_ids_alive

        phases = data["phases"] - data["frames"][0]

        super().__init__(tstamps=tstamps, codes_ids=codes_ids, get_period_and_minute=get_period_and_minute, fps=data["meta"]["FrameRate"], goals_tstamps=[goals_scored_tstamps, goals_conceded_tstamps], phases=phases, ball_position=ball_position, roles = roles, players_jersie_number=players_jersie_number)

        self.N = None

    def copy(self):
        return deepcopy(self)
    
    @staticmethod
    def codes_to_ids(codes: np.ndarray):
        ids=np.zeros(len(codes), dtype=TeamNet.uint_dtype)
        ptr = 0
        for b, c in zip(TeamNet.bits, codes.T):
            ids+=((c.astype(TeamNet.uint_dtype)<<(TeamNet.bits_dtype-b))>>(TeamNet.bits_dtype-b))<<ptr
            ptr+=b
        return ids

    @staticmethod
    def ids_to_codes(ids: np.ndarray): 
        codes = np.zeros((len(TeamNet.bits),len(ids)), dtype=TeamNet.int_dtype)
        ids_ = ids.copy()
        for i, n_bit in enumerate(TeamNet.bits):
            n_clear = (TeamNet.bits_dtype-n_bit)
            codes[i] = (ids_.astype(TeamNet.int_dtype)<<n_clear)>>n_clear if i==0 else (ids_<<n_clear)>>n_clear
            ids_ >>= n_bit
        return codes.T


class CartesianNet(BaseNet):
    bits_dtype = TeamNet.bits_dtype * 2
    uint_dtype = f"uint{bits_dtype}"
    int_dtype = f"int{bits_dtype}"

    bits = TeamNet.bits + TeamNet.bits
    tot_bits = sum(bits)

    assert tot_bits <= bits_dtype

    goal_names = ["away_team", "home team"]


    def init_super(self, **kwargs):
        roles = np.append(self.away_team.roles, self.home_team.roles, axis = 1)
        players_jersie_number = np.append(self.away_team_.players_jersie_number, self.home_team_.players_jersie_number)
        super().__init__(tstamps=self.home_team._tstamps, get_period_and_minute=self.home_team.get_period_and_minute, fps=self.home_team.fps, phases=self.home_team.phases, goals_tstamps=self.away_team.goals_tstamps,  ball_position=self.away_team.ball_position, roles = roles, players_jersie_number=players_jersie_number, **kwargs)

    def __init__(self, away_team_net: TeamNet, home_team_net: TeamNet, **kwargs):
        self.home_team_ = home_team_net.copy()
        self.away_team_ = away_team_net.copy()
        self.goal_names = [away_team_net.team_name, home_team_net.team_name]
        if not "codes_ids" in kwargs:
            kwargs["codes_ids"] = CartesianNet.pack_ids(ids_away = self.away_team._codes_ids, ids_home = self.home_team._codes_ids)
        self.init_super(**kwargs)
    @property
    def away_team(self):
        return self.away_team_.copy()
    
    @property
    def home_team(self):
        return self.home_team_.copy()
    
    @property
    def teams(self):
        return [self.away_team, self.home_team]


    @staticmethod
    def possession_str(code):
        return ["A", "H"][code[1,-1]]

    @staticmethod
    def stringify(code):
        return "/".join([CartesianNet.possession_str(code)]+[CartesianNet.get_formation_str(c) for c in code])

    @staticmethod
    def pack_ids(ids_away, ids_home):
        return (ids_home.astype(CartesianNet.uint_dtype) << TeamNet.bits_dtype) + ids_away.astype(CartesianNet.uint_dtype)
    
    @staticmethod
    def unpack_ids(ids):
        l=(CartesianNet.bits_dtype-TeamNet.bits_dtype)
        return {
            "home": (ids>>TeamNet.bits_dtype).astype(TeamNet.uint_dtype),
            "away": (ids<<l>>l).astype(TeamNet.uint_dtype)
        }

    @staticmethod
    def codes_to_ids(codes: np.ndarray):
        id_home = TeamNet.codes_to_ids(codes[:,0])
        id_away = TeamNet.codes_to_ids(codes[:,1])

        ids = CartesianNet.pack_ids(ids_away=id_away, ids_home=id_home)
        return ids


    @staticmethod
    def ids_to_codes(ids: np.ndarray): 
        ids_dict = CartesianNet.unpack_ids(ids)
        codes_home = TeamNet.ids_to_codes(ids_dict["home"])
        codes_away = TeamNet.ids_to_codes(ids_dict["away"])

        return np.stack([codes_away, codes_home], axis=1)
    
    @property
    def possession_edges(self):
        diff = np.diff(self.edges_codes[:,:,0], axis=1)[:,0, -1]
        mask = np.abs(diff)==1
        return mask
    
    def node_data(self, code, *args, **kwargs):
        data = {}
        for t, team in enumerate(["away", "home"]):
            data |= super().node_data(code[t], prefix=team)
        return data

    def get_node_kwargs(self, *args, **kwargs):
        kwargs = super().get_node_kwargs(*args, **kwargs)
        kwargs["home vs away offensive ratio"] = ((kwargs["home offensive ratio"] - kwargs["away offensive ratio"])+1)/2
        return kwargs

    def add_edges_roles(self, kwargs, edge_roles):
        def set_role(team_name, i , n):
            kwargs[f"start {team_name} {n} role"], kwargs[f"end {team_name} {n} role"] = edge_roles[:, i]
            kwargs[f"change {team_name} {n} role"] = " to ".join([str(r) for r in edge_roles[:, i]])

        team_name = self.away_team.team_name
        for i, n in enumerate(self.away_team.players_jersie_number):
            set_role(team_name, i, n)
        team_name = self.home_team.team_name
        for i, n in enumerate(self.home_team.players_jersie_number):
            set_role(team_name, i+len(self.away_team.players_jersie_number), n)



class CartesianFilteredNet(CartesianNet):
    def __init__(self, away_team: TeamNet,home_team: TeamNet, split_trans_edges = True, **kwargs):
        n_tstamps=len(home_team._codes_ids)
        assert n_tstamps == len(away_team._codes_ids)

        teams_codes_ids = np.zeros((2, n_tstamps))
        loops = np.ones(n_tstamps, dtype=np.bool_)

        for t, team in enumerate([away_team, home_team]):
            team_loop = team.is_loop
            last_te = 0
            if split_trans_edges:
                for i, (ts, te) in enumerate(team.edges_tstamps):
                    teams_codes_ids[t, ts:(te+ts)//2] = team.edges_ids[i,0]
                    teams_codes_ids[t, (ts+te)//2:te+1] = team.edges_ids[i,1]

                continue
            team = team.add_zero_loops()
            for i in np.flatnonzero(team_loop):
                ts, te = team.edges_tstamps[i]
                loops[last_te:ts] = False
                teams_codes_ids[t, ts:te+1] = team.edges_ids[i, 0]
                last_te = te

        kwargs["codes_ids"] = CartesianFilteredNet.pack_ids(*teams_codes_ids)
        
        super().__init__(away_team, home_team, **kwargs)

        self.collapse_loops(inplace=True, **kwargs)
        mask = self.adj_to_zero_id()
        self.collapse_edges(mask, inplace=True, **kwargs)


    
    def adj_to_zero_id(self):
        mask = np.zeros_like(self.edges_weights, np.bool_)
        for ax in self.edges_ids.T:
            ids = self.unpack_ids(ax)
            for id in ids.values():
                mask |= id == 0 # Be aware that if teams' networks have some ids = 0, this can introduce some conflicts!
        return mask
        




class BothTeamCartesianNet(CartesianNet):
    def __init__(self, match_id, **kwargs):
        teams = []
        for i in range(2):
            teams.append(TeamNet(match_id, team=i, **kwargs))
        super().__init__(*teams)




class BothTeamCartesianFilteredNet(CartesianFilteredNet):
    def __init__(self, match_id, operations: tuple | list, *args, **kwargs):
        teams = []
        for i in range(2):
            team_net = TeamNet(match_id, team=i, *args, **kwargs)
            for op in operations:
                op_args = []
                op_kwargs = {}
                op_name = op
                
                    
                if isinstance(op, tuple | list) and len(op)>1:
                    op_name = op[0]
                    for op_arg_group in op[1:]:
                        if isinstance(op_arg_group, tuple | list):
                            op_args = op_arg_group
                        elif isinstance(op_arg_group, dict):
                            op_kwargs = op_arg_group
                
                if callable(op_name):
                    op_name(team_net, inplace=True, *op_args, **op_kwargs)
                else:
                    getattr(team_net, op_name).__call__(inplace=True, *op_args, **op_kwargs)

                
            teams.append(team_net)
        super().__init__(*teams, *args, **kwargs)

    
if __name__=="__main__":
    teams = []
    algorithm = "barycenter"

    threshold_value = 50
    threshold_value_team = {
        "barycenter": [60],
        "dummy": [80]
    }[algorithm]

    thresholds_deg = {
        "barycenter": [15],
        "dummy": [10]
    }[algorithm]
    thresholds_deg_cart = [1,5]

    match_id=56

    operations=[]

    for t in threshold_value_team:
        operations = [
            (TeamNet.remove_loops, {"threshold": t}),
        ]

    for t in thresholds_deg:
        operations += [
            (TeamNet.remove_small_degree, {"threshold": t, "iterate": False}),
        ]

    bt_cn = BothTeamCartesianFilteredNet(match_id=match_id, operations=operations, algorithm=algorithm, split_trans_edges=False, xml=False)

    export_fname = lambda f: join(settings.get_data_match_folder_path(settings.get_match_folder(match_id)),f)

    for team in bt_cn.teams:     
        team_name = team.team_name       
        elements_name = [
            "network_team",
            str(team_name),
            *[f"t{t}" for t in threshold_value_team],
            *[f"g{t}" for t in thresholds_deg],
            algorithm,
        ]
        elements_name[-1] += ".graphml"
        fname = "_".join(elements_name)
        team.export(export_fname(fname), remove_possession_edges=False)

