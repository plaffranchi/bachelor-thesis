from threading import Thread, Lock
from utils import ProgressBar
import numpy as np
from scipy.spatial import Delaunay
import time


def compute_formations_process(coords, on_field, threshold, i, n_threads, verbose, progress_bar: None | ProgressBar):
    l = coords.shape[0]

    codes = np.zeros((l, 5), dtype=np.int8)
    process_edges = np.zeros((l, 10, 10), dtype=np.int8)

    n_players = coords.shape[1]

    thread_lock = Lock()

    roles = np.zeros(on_field.shape) - 1

    def compute_formations_thread(j):
        ts = time.time()

        for i_e in range(j, l, n_threads):
            edges = np.zeros((10, 10), dtype=np.int16)

            of = on_field[i_e]

            pts = coords[i_e, of]


            try:
                dnay = Delaunay(pts)
            except:
                codes[i_e, 0] = -1
                roles[i_e, of] = -1
                continue


            simp = (dnay.simplices)
            edges[simp[:,0],simp[:,1]] = simp[:,2]+1 
            edges[simp[:,2],simp[:,0]] = simp[:,1]+1 
            edges[simp[:,1],simp[:,2]] = simp[:,0]+1 


            one_opp_only = np.triu((edges.T > 0) & (edges==0))

            one_opp_only_triu = np.triu(one_opp_only)

            edges[one_opp_only_triu] = edges.T[one_opp_only_triu]
            edges.T[one_opp_only_triu] = 0


            edges_ixs = np.array(np.nonzero(edges))
            triag_edges_ixs = edges_ixs[:, edges_ixs[1] > edges_ixs[0]]

            edges_2_opp_ixs_mask = edges[triag_edges_ixs[1],
                                         triag_edges_ixs[0]] != 0

            edges_2_opp_ixs = triag_edges_ixs[:, edges_2_opp_ixs_mask]
            edges_1_opp_ixs = triag_edges_ixs[:, ~edges_2_opp_ixs_mask]

            # Remove edge at the borders

            p, q = edges_1_opp_ixs
            a = edges[p, q] - 1

            u, v, u_ = pts[[p, q, a]]

            uv = v - u
            u_u = u - u_

            m = np.stack([uv, u_u], axis=1)

            det = np.linalg.det(m)
            delta = np.abs(det) / (2*np.linalg.norm(uv, axis=1))

            mask = delta > threshold

            edges[p, q] = mask

            # Remove edges with two opposite vertices

            p, q = edges_2_opp_ixs
            a, b = edges[p, q] - 1, edges[q, p] - 1

            u_, v_, u, v = pts[[b, a, p, q]]

            uv = v - u
            s = (u + v)/2
            u_v_ = v_ - u_

            inv_mat = np.linalg.inv(np.stack([np.stack([-uv[:, 1], -u_v_[:, 1]], axis=1), np.stack([uv[:, 0], u_v_[:, 0]], axis=1)], axis=1))[:, 0]

            t = (u_ + v_ - u - v)/2

            lamb = inv_mat[:, 0] * t[:, 0] + inv_mat[:, 1] * t[:, 1]

            lamb = np.stack([lamb, lamb], axis=1)

            c = c = lamb * np.stack([-uv[:, 1], uv[:, 0]], axis=1) + s

            d_vec_norm = np.linalg.norm(u - c, axis=1)
            D_vec_norm = np.linalg.norm(u_ - c, axis=1)

            delta = (D_vec_norm - d_vec_norm)/2

            mask = delta > threshold

            edges[p, q] = mask

            process_edges[i_e] = edges

            edges_ixs = np.array(np.nonzero(edges))
            triag_edges_ixs = edges_ixs[:, edges_ixs[1] > edges_ixs[0]]

            angles = np.arctan2(pts[triag_edges_ixs[1], 1] - pts[triag_edges_ixs[0], 1],
                                pts[triag_edges_ixs[1], 0] - pts[triag_edges_ixs[0], 0]) + np.pi
            angles_opp = (angles + np.pi) % (2*np.pi)

            angles = np.concatenate((angles, angles_opp))

            triag_edges_ixs_opp = np.array(
                [triag_edges_ixs[1], triag_edges_ixs[0]])
            ixs = np.concatenate(
                (triag_edges_ixs, triag_edges_ixs_opp), axis=1)

            def sorted_neighs(v, shift_angle):
                shift_angle %= 2*np.pi
                a = ((angles - shift_angle + np.pi) % (2*np.pi))
                ab_order = np.argsort(a)
                ixs_ = ixs.T[ab_order]
                return ixs_[ixs_[:, 0] == v, 1]

            def next_edge(v, u, d):
                neighs = sorted_neighs(v, 0)
                edge = np.argwhere(neighs == u)
                if not len(edge):
                    return -1
                i = edge[0, 0]
                return neighs[(i+d) % len(neighs)]

            def navigate(v, v_, up, arr, d):
                arr[v] = True
                # def compare(a, b): return a < b if up else a > b
                thresh_angle = np.pi/8
                def compare(a, b): 
                    alpha = np.arctan2(b[1]-a[1], b[0]-a[0])
                    mirror = (2*int(up)-1)
                    alpha *= mirror
                    shift = thresh_angle*(d*mirror+1)/(2)
                    alpha+=shift
                    return thresh_angle < alpha < np.pi

                while compare(pts[v], pts[v_]):
                    v, v_ = v_, next_edge(v_, v, d)
                    arr[v] = True
                return v

            def get_tb(func):
                return np.flatnonzero(func(pts[:, 0]) == pts[:, 0])

            pos = roles[i_e, of]

            backline = np.zeros(pos.shape[0], dtype=np.bool_)
            frontline = np.zeros(pos.shape[0], dtype=np.bool_)

            bottoms = get_tb(np.amin)
            lmost_back = lmost_front = np.argmin(pts[:, 1]) # Take the rightmost to make sure it gets updated
            rmost_back = rmost_front = np.argmax(pts[:, 1]) # Take the leftmost to make sure it gets updated

            undef_code = False

            for bottom in bottoms:
                v = bottom
                n_bottom = sorted_neighs(v, -np.pi/2)

                if not len(n_bottom):
                    undef_code = True
                    break

                v_ = n_bottom[-1]

                lmost_back_ = navigate(v, v_, True, backline, -1)
                if pts[lmost_back_,1] > pts[lmost_back,1]:
                    lmost_back = lmost_back_


                v = bottom
                v_ = n_bottom[0]
                rmost_back_ = navigate(v, v_, False, backline, 1)
                if pts[rmost_back_,1] < pts[rmost_back,1]:
                    rmost_back = rmost_back_

            if not undef_code:
                tops = get_tb(np.amax)

                for top in tops:
                    v = top
                    n_top = sorted_neighs(v, np.pi/2)

                    if not len(n_top):
                        undef_code = True
                        break

                    v_ = n_top[-1]
                    rmost_front_ = navigate(v, v_, False, frontline, -1)
                    if pts[rmost_front_,1] < pts[rmost_front,1]:
                        rmost_front = rmost_front_


                    v = top
                    v_ = n_top[0]
                    lmost_front_ = navigate(v, v_, True, frontline, 1)
                    if pts[lmost_front_,1] > pts[lmost_front,1]:
                        lmost_front = lmost_front_
            if undef_code:
                codes[i_e, 0] = -1
                roles[i_e, of] = -1
                continue


            pos[(~frontline) & (~backline)] = 2
            pos[backline] = 0
            pos[frontline] = 4
            if lmost_back == lmost_front:
                pos[lmost_back] = 1

            if rmost_back == rmost_front:
                pos[rmost_back] = 3

            for c_i in range(5):
                codes[i_e, c_i] = (pos == c_i).sum()

            roles[i_e, of] = pos

            if progress_bar:
                with thread_lock:
                    if progress_bar.is_to_update((i_e-j)/n_threads, verbose):
                        progress_bar.print()

    threads = []

    for j in range(n_threads):
        thread = Thread(target=compute_formations_thread,
                        args=(j,))
        threads.append(thread)
        thread.start()

    for t in threads:
        t.join()

    return [codes, roles, process_edges]
